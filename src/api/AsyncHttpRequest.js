import axios from "axios"

export const getData = async (url, callback) => {
  try {
    const { data } = await axios.get(url)
    callback(data.data)
  } catch (error) {
    console.log(error.message)
  }
}

export const post = async (url, formData = null, callBack = null) => {
  try {
    const data = await axios.post(url, formData)
    callBack(data)
  } catch (error) {
    if (callBack !== null) {
      callBack(error)
    }
    console.log(error)
  }
}

export const put = async (url, formData = null, callBack) => {
  try {
    const data = await axios.put(url, formData)
    callBack(data)
  } catch (error) {
    callBack(error)
  }
}

// export function remove(url, params = null, callBack = null) {
//   return axios.delete(url, params)
// }
