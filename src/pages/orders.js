import React from "react"
import Sidebar from "../components/Sidebar/Sidebar"
import Seo from "../components/seo"
import Orders from "../modules/Orders/Orders"

export default function SimpleTabs() {
  return (
    <Sidebar>
      <Seo title="Seccion de ordenes" />
      <Orders />
    </Sidebar>
  )
}
