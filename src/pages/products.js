import React from "react"
import Sidebar from "../components/Sidebar/Sidebar"
import Seo from "../components/seo"
import Products from "../modules/products/Products"

export default function SimpleTabs() {
  return (
    <Sidebar>
      <Seo title="Productos" />
      <Products />
    </Sidebar>
  )
}
