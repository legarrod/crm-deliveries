import React from "react"
import Sidebar from "../components/Sidebar/Sidebar"
import Seo from "../components/seo"
import Vendors from "../modules/vendors/Vendors"

export default function SimpleTabs() {
  return (
    <Sidebar>
      <Seo title="Clientes" />
      <Vendors />
    </Sidebar>
  )
}
