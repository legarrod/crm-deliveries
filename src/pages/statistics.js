import React from "react"
import Seo from "../components/seo"
import StatisticsComponent from "../modules/statistics/StatisticsComponent"

import Sidebar from "../components/Sidebar/Sidebar"

export default function Statistics() {
  return (
    <Sidebar>
      <Seo title="Estadisticas" />
      <StatisticsComponent />
    </Sidebar>
  )
}
