import React from "react"
import Sidebar from "../components/Sidebar/Sidebar"
import Seo from "../components/seo"
import Customer from "../modules/customer/Customers"

export default function SimpleTabs() {
  return (
    <Sidebar>
      <Seo title="Clientes" />
      <Customer />
    </Sidebar>
  )
}
