import React from "react"
import Sidebar from "../components/Sidebar/Sidebar"
import Seo from "../components/seo"
import Documentation from "../modules/documentation/Documentation"

function IndexPage() {
  return (
    <Sidebar>
      <Seo title="DOCUMENTACION | CRM" />
      <Documentation />
    </Sidebar>
  )
}

export default IndexPage
