import React, { useEffect, useCallback } from "react"
import InformationHome from "../components/InformationHome/InformationHome"
import Sidebar from "../components/Sidebar/Sidebar"
import Seo from "../components/seo"
import Header from "../components/Header/header"

function IndexPage() {
  const axios = require("axios")
  //http://apismartgo.crmsystem.tech/
  //http://apicrm.recaudo.xyz
  const getData = useCallback(
    async (url, setData = null) => {
      try {
        const { data } = await axios.get(
          "https://pokeapi.co/api/v2/pokemon/ditto"
        )
        console.log(data)
      } catch (error) {
        console.log(error.message)
      }
    },
    [axios]
  )
  useEffect(() => {
    getData()
  }, [getData])
  return (
    <Sidebar>
      <Seo title="INICIO | CRM" />
      <Header siteTitle={"CRM SmartGo"} />
      <InformationHome />
    </Sidebar>
  )
}

export default IndexPage
