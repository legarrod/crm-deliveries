import React from "react"
import Sidebar from "../components/Sidebar/Sidebar"
import Seo from "../components/seo"
import Deliver from "../modules/Orders/Deliver"

export default function SimpleTabs() {
  return (
    <Sidebar>
      <Seo title="Seccion de ordenes" />
      <Deliver />
    </Sidebar>
  )
}
