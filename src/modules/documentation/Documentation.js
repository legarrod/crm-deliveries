import React from "react"
import { withStyles } from "@material-ui/core/styles"
import MuiAccordion from "@material-ui/core/Accordion"
import MuiAccordionSummary from "@material-ui/core/AccordionSummary"
import MuiAccordionDetails from "@material-ui/core/AccordionDetails"
import Typography from "@material-ui/core/Typography"
import imageCsvCustmer from "../../assets/images/ejemplo-customer.png"
import imageCsvProductos from "../../assets/images/ejemplo-productos.png"
import imageCsvEntradas from "../../assets/images/ejemplo-inputs.png"

const Accordion = withStyles({
  root: {
    border: "1px solid rgba(0, 0, 0, .125)",
    boxShadow: "none",
    "&:not(:last-child)": {
      borderBottom: 0,
    },
    "&:before": {
      display: "none",
    },
    "&$expanded": {
      margin: "auto",
    },
  },
  expanded: {},
})(MuiAccordion)

const AccordionSummary = withStyles({
  root: {
    backgroundColor: "rgba(0, 0, 0, .03)",
    borderBottom: "1px solid rgba(0, 0, 0, .125)",
    marginBottom: -1,
    minHeight: 56,
    "&$expanded": {
      minHeight: 56,
    },
  },
  content: {
    "&$expanded": {
      margin: "12px 0",
    },
  },
  expanded: {},
})(MuiAccordionSummary)

const AccordionDetails = withStyles(theme => ({
  root: {
    padding: theme.spacing(2),
  },
}))(MuiAccordionDetails)

export default function Documentation() {
  const [expanded, setExpanded] = React.useState("panel1")

  const handleChange = panel => (event, newExpanded) => {
    setExpanded(newExpanded ? panel : false)
  }

  return (
    <div className="mx-5 my-9 flex flex-wrap justify-center items-center">
      <h1 className="w-full text-center text-xl sm:text-4xl font-semibold my-4">
        TODO SOBRE NUESTRO SISTEMA
      </h1>
      <Accordion
        square
        expanded={expanded === "panel1"}
        onChange={handleChange("panel1")}
      >
        <AccordionSummary aria-controls="panel1d-content" id="panel1d-header">
          <Typography>Format de los CSV a subir</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <div className="w-full flex flex-col justify-center mt-2">
            <p className="text-xs text-red-800">
              <b>NOTA IMPORTATE:</b> ningun archivo CSV debe contener en la fila
              1 las cabeceras de las columnas
            </p>
            <h2 className="w-full text-xl font-semibold">
              Formato de para subir un archivo csv con varios clientes
            </h2>
            <img
              src={imageCsvCustmer}
              className="w-full lg:w-3/4"
              alt="Imagen de ejemplo csv customers"
            />
            <p>
              Se debe subir como se ve en la imagen, la <b>columna A</b> es el
              tipo de documento, la <b>columna B</b> es el tipo de cliente (COR
              = Corporativo, NOR = Normal), la <b>columna C</b> es el numero de
              documento, <b>columna D</b>
              nombre completo, <b>columna E</b> Direccion, <b>Columna F</b>{" "}
              correo electronico, <b>columna G</b> numero de telefono y la{" "}
              <b>columna H </b>es el numero de dias que el cliente autoriza para
              hacer llamdas de seguimiento.
            </p>
            <h2 className="w-full text-xl font-semibold mt-2">
              Formato de para subir un archivo csv con varios productos
            </h2>
            <img
              src={imageCsvProductos}
              className="w-full lg:w-3/4"
              alt="Imagen de ejemplo csv customers"
            />
            <p>
              Se debe subir como se ve en la imagen, la <b>columna A</b> es el
              SKU, la <b>columna B</b> es el nombre del producto, la{" "}
              <b>columna C</b> es el link o url de la imagen del producto,{" "}
              <b>columna D</b>
              es una descripcion corta del producto.
            </p>
            <h2 className="w-full text-xl font-semibold mt-2">
              Formato de para subir un archivo csv con entradas deproductos
            </h2>
            <img
              src={imageCsvEntradas}
              className="w-full lg:w-3/4"
              alt="Imagen de ejemplo csv customers"
            />
            <p>
              Se debe subir como se ve en la imagen, la <b>columna A</b> es la
              cantidad de productos, la <b>columna B</b> es el costo del
              producto, la <b>columna C</b> es el SKU del producto.
            </p>
          </div>
        </AccordionDetails>
      </Accordion>
      <Accordion
        square
        expanded={expanded === "panel2"}
        onChange={handleChange("panel2")}
      >
        <AccordionSummary aria-controls="panel2d-content" id="panel2d-header">
          <Typography>Collapsible Group Item #2</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
            malesuada lacus ex, sit amet blandit leo lobortis eget. Lorem ipsum
            dolor sit amet, consectetur adipiscing elit. Suspendisse malesuada
            lacus ex, sit amet blandit leo lobortis eget.
          </Typography>
        </AccordionDetails>
      </Accordion>
      <Accordion
        square
        expanded={expanded === "panel3"}
        onChange={handleChange("panel3")}
      >
        <AccordionSummary aria-controls="panel3d-content" id="panel3d-header">
          <Typography>Collapsible Group Item #3</Typography>
        </AccordionSummary>
        <AccordionDetails>
          <Typography>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse
            malesuada lacus ex, sit amet blandit leo lobortis eget. Lorem ipsum
            dolor sit amet, consectetur adipiscing elit. Suspendisse malesuada
            lacus ex, sit amet blandit leo lobortis eget.
          </Typography>
        </AccordionDetails>
      </Accordion>
    </div>
  )
}
