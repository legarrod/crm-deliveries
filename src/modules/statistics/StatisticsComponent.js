import React, { useEffect, useState, useCallback } from "react"
import { getData } from "../../api/AsyncHttpRequest"
import SentimentVeryDissatisfiedIcon from "@material-ui/icons/SentimentVeryDissatisfied"
import StatisticsRound from "./CustomerRound"
import OrderResume from "./OrderResume"
import SalesMonth from "./SalesMonth"

export default function StatisticsComponent() {
  const urlAllCustomers = `http://apismartgo.crmsystem.tech/public/bussinessorders/api/bussinessorders/statistics`
  const [data, setData] = useState(null)
  const [buscando, setBuscando] = useState(true)

  const respponseCallback = useCallback(response => {
    setBuscando(false)
    if (response.length >= 0) {
      setData(response?.[0])
    } else {
      setData({})
    }
  }, [])

  useEffect(() => {
    if (buscando) {
      getData(`${urlAllCustomers}`, respponseCallback)
    }
  }, [urlAllCustomers, respponseCallback, buscando])
  return (
    <div>
      {data !== null ? (
        <div
          position="primary"
          className={`bg-gray-900 pt-5 flex flex-wrap justify-center items-center`}
        >
          <div className="flex flex-col justify-center items-center mb-4">
            <OrderResume data={data} />
            <StatisticsRound dataIn={data} />
          </div>

          <SalesMonth dataIn={data} />
        </div>
      ) : (
        <div className="bg-gray-900 pt-5 flex h-screen flex-col justify-center items-center">
          <SentimentVeryDissatisfiedIcon
            style={{ fontSize: "100px", color: "#ffff" }}
          />{" "}
          <h2 className="text-2xl text-white text-center">
            LO SENTIMOS NO HAY INFORMACION DE CLIENTES
          </h2>
        </div>
      )}
    </div>
  )
}
