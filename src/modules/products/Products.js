import React from "react"
import PropTypes from "prop-types"
import { makeStyles } from "@material-ui/core/styles"
import AppBar from "@material-ui/core/AppBar"
import Tabs from "@material-ui/core/Tabs"
import Tab from "@material-ui/core/Tab"
import Typography from "@material-ui/core/Typography"
import Box from "@material-ui/core/Box"
import ListProducts, { FormUpdate } from "./ListProducts"
import FormCreateProduct from "./FormCreateProduct"
import { Alert, AlertTitle } from "@material-ui/lab"
//import FormCreateInput from "./FormCreateInput"
import Inventory from "../inventory/Inventory"
function TabPanel(props) {
  const { children, value, index, ...other } = props

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={4}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  )
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
}

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
  estiloTabs: {
    backgroundColor: "black",
    color: "#59D741",
  },
}))

export default function Products() {
  const classes = useStyles()
  const [value, setValue] = React.useState(0)
  const [state, setState] = React.useState({
    viewFormUpdate: false,
    data: {},
    succes: false,
    error: false,
    change: false,
  })

  function a11yProps(index) {
    return {
      id: `simple-tab-${index}`,
      "aria-controls": `simple-tabpanel-${index}`,
    }
  }

  const handleChange = (event, newValue) => {
    setValue(newValue)
  }

  return (
    <div>
      <div className={`mt-8 px-2 sm:px-9 ${classes.root}`}>
        <AppBar
          position="static"
          classes={{
            root: classes.estiloTabs,
          }}
        >
          <Tabs
            value={value}
            onChange={handleChange}
            aria-label="simple tabs example"
          >
            <Tab
              label="Actualizr costo producto"
              {...a11yProps(0)}
              onClick={() => setState({ viewFormUpdate: false })}
            />
            <Tab
              label="Crear producto / subir"
              {...a11yProps(1)}
              onClick={() => setState({ viewFormUpdate: false })}
            />
            {/* <Tab
              label="Crear entradas"
              {...a11yProps(2)}
              onClick={() => setState({ viewFormUpdate: false })}
            /> */}
            <Tab label="Productos" {...a11yProps(2)} />
          </Tabs>
        </AppBar>
        <TabPanel value={value} index={0}>
          <Inventory />
        </TabPanel>
        <TabPanel value={value} index={1}>
          <FormCreateProduct />
        </TabPanel>
        {/* <TabPanel value={value} index={2}>
          <FormCreateInput />
        </TabPanel> */}
        <TabPanel value={value} index={2}>
          <ListProducts setState={setState} />
        </TabPanel>
      </div>
      {state.viewFormUpdate && <FormUpdate data={state} setState={setState} />}
      {state.succes && (
        <div className={classes.root}>
          <Alert severity="success">
            <AlertTitle>Excelente</AlertTitle>
            Información actualizada — <strong>exitosamente!</strong>
          </Alert>
        </div>
      )}
      {state.error && (
        <div className={classes.root}>
          <Alert severity="error">
            <AlertTitle>Algo salio mal</AlertTitle>
            La información no se pudo actualizar —{" "}
            <strong>intenta nuevamente!</strong>
          </Alert>
        </div>
      )}
    </div>
  )
}
