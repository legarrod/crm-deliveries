import React, { useState, useEffect } from "react"
import { getData, put } from "../../api/AsyncHttpRequest"
import { DataGrid } from "@material-ui/data-grid"
import CreateIcon from "@material-ui/icons/Create"
import DeleteForeverIcon from "@material-ui/icons/DeleteForever"
import SentimentVeryDissatisfiedIcon from "@material-ui/icons/SentimentVeryDissatisfied"
import { useForm } from "react-hook-form"
import getDate from "../../utils/getDate"
import Button from "@material-ui/core/Button"
import swal from "sweetalert"

export function FormUpdate({ data, setState }) {
  let hoy = new Date()
  const urlSaveNewCustomer = `http://apismartgo.crmsystem.tech/public/products/api/products/update`
  const { register, handleSubmit } = useForm()
  let sku = data?.data?.sku
  const callbackSaveCustomer = response => {
    if (response?.data?.data === true) {
      setState({
        succes: true,
        error: false,
        change: false,
      })
    } else {
      setState({
        succes: false,
        error: true,
        change: false,
      })
    }

    setTimeout(() => {
      setState({
        succes: false,
        error: false,
        change: false,
      })
    }, 20000)
  }

  const onSubmit = data => {
    let newData = {
      creation_date: getDate(hoy),
      name_product: data.name_product,
      sku: sku,
      photo_link:
        data.photo_link ||
        "https://upload.wikimedia.org/wikipedia/commons/thumb/d/da/Imagen_no_disponible.svg/1200px-Imagen_no_disponible.svg.png",
      descriptionProd: data.descriptionProd,
    }
    swal({
      title: "Estas seguro?",
      text: `que quieres actualizar este producto? ${data.name_product}`,
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then(willDelete => {
      if (willDelete) {
        put(urlSaveNewCustomer, newData, callbackSaveCustomer)
      }
    })
  }

  return (
    <>
      <form
        className="flex flex-wrap justify-center m-5 items-center border-2 p-5 border-gray-500 rounded-xl"
        onSubmit={handleSubmit(onSubmit)}
      >
        <div className="mx-2 my-2">
          <input
            tabindex="1"
            className="border-2 border-gray-400 w-56 rounded-md m-1 text-lg p-1"
            defaultValue={data?.data?.name_product}
            placeholder="Nombre"
            {...register("name_product", { required: true, maxLength: 50 })}
          />
        </div>
        <div className="mx-2 my-2">
          <input
            tabindex="2"
            className="border-2 border-gray-400 text-gray-500 w-56 rounded-md m-1 text-lg p-1"
            defaultValue={data?.data?.sku}
            placeholder={data?.data?.sku}
            disabled
          />
        </div>
        <div className="mx-2 my-2">
          <input
            tabindex="3"
            className="border-2 border-gray-400 w-56 rounded-md m-1 text-lg p-1"
            defaultValue={data?.data?.photo_link}
            placeholder="Url imagen"
            {...register("photo_link", {
              maxLength: 200,
            })}
          />
        </div>
        <div className="mx-2 my-2">
          <input
            tabindex="4"
            className="border-2 border-gray-400 w-56 rounded-md m-1 text-lg p-1"
            defaultValue={data?.data?.descriptionProd}
            placeholder="Descripcion"
            label="Descripcion"
            {...register("descriptionProd", { maxLength: 200 })}
          />
        </div>

        <div className="mx-2 my-2">
          <Button type="submit" variant="contained" color="primary">
            Guardar
          </Button>
        </div>
      </form>
    </>
  )
}

export default function DataTable({ setState }) {
  const urlAllCustomers = `http://apismartgo.crmsystem.tech/public/products/api`
  const [rows, setRows] = useState([])
  const [buscando, setBuscando] = useState(true)

  const respponseCallback = response => {
    setBuscando(false)
    setRows(response)
  }

  const respponseCallbackEdit = response => {
    if (response.length === 1) {
      setState({ viewFormUpdate: true, data: response[0] })
      getData(`${urlAllCustomers}/products`, respponseCallback)
    }
  }
  const respponseCallbackDelete = response => {
    if (response) {
      setState({ succes: true })
      getData(`${urlAllCustomers}/products`, respponseCallback)
    }
  }

  const handlerEdit = e => {
    setState({ change: true })
    getData(`${urlAllCustomers}/productsid/${e}`, respponseCallbackEdit)
  }

  const handlerDelet = e => {
    swal({
      title: "Estas seguro?",
      text: `que quieres eliminar este producto? ${e?.row?.name_product}`,
      icon: "warning",
      buttons: true,
      dangerMode: true,
    }).then(willDelete => {
      if (willDelete) {
        put(
          `${urlAllCustomers}/products/delete`,
          {
            id: e.value,
            state_products: 0,
          },
          respponseCallbackDelete
        )
      }
    })
  }

  const columns = [
    {
      field: "id",
      headerName: "",
      width: 150,
      description:
        "Que desea hacer, editar eliminar o guardar fecha de ultima llamada.",
      renderCell: params => (
        <div>
          <button
            onClick={() => handlerEdit(params.value)}
            className="m-0 mr-1"
            variant="contained"
            color="primary"
            size="small"
            style={{ marginLeft: 5 }}
          >
            <CreateIcon />
          </button>
          <button
            onClick={() => handlerDelet(params)}
            className="m-0 mr-1"
            variant="contained"
            color="primary"
            size="small"
            style={{ marginLeft: 5 }}
          >
            <DeleteForeverIcon />
          </button>
        </div>
      ),
    },
    {
      field: "sku",
      headerName: "SKU",
      width: 120,
    },
    { field: "name_product", headerName: "Nombre", width: 230 },
    {
      field: "photo_link",
      headerName: "Foto",
      type: "number",
      width: 100,
      renderCell: params => (
        <img
          src={params.value}
          alt="CRM"
          className="w-10 h-10"
          style={{ objectFit: "cover" }}
        />
      ),
    },
    {
      field: "descriptionProd",
      headerName: "Descripcion",
      description: "This column has a value getter and is not sortable.",
      sortable: false,
      width: 300,
    },
  ]

  useEffect(() => {
    if (buscando) {
      getData(`${urlAllCustomers}/products`, respponseCallback)
    }
  }, [urlAllCustomers, respponseCallback, buscando])

  return (
    <div style={{ height: 400, width: "100%" }}>
      {rows?.length > 0 ? (
        <DataGrid rows={rows} columns={columns} pageSize={10} />
      ) : (
        <div className="flex flex-col justify-center items-center">
          <h2 className="text-2xl text-center">
            LO SENTIMOS NO HAY INFORMACION DE PRODUCTOS
          </h2>
          <SentimentVeryDissatisfiedIcon style={{ fontSize: "50px" }} />{" "}
        </div>
      )}
    </div>
  )
}
