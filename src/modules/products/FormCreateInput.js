import React, { useState, useEffect, useCallback } from "react"
import { useForm } from "react-hook-form"
import Button from "@material-ui/core/Button"
import getDate from "../../utils/getDate"
import { makeStyles } from "@material-ui/core/styles"
import { post, getData } from "../../api/AsyncHttpRequest"
import { Alert, AlertTitle } from "@material-ui/lab"
import Checkbox from "@material-ui/core/Checkbox"
import AddCircleIcon from "@material-ui/icons/AddCircle"
import Table from "@material-ui/core/Table"
import TableBody from "@material-ui/core/TableBody"
import TableCell from "@material-ui/core/TableCell"
import TableContainer from "@material-ui/core/TableContainer"
import TableHead from "@material-ui/core/TableHead"
import TableRow from "@material-ui/core/TableRow"
import Paper from "@material-ui/core/Paper"
import DeleteForeverIcon from "@material-ui/icons/DeleteForever"
import { navigate } from "gatsby"

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    "& > * + *": {
      marginTop: theme.spacing(2),
    },
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 200,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  paper: {
    position: "absolute",
    width: 400,
    backgroundColor: "#ffffff",
    border: "2px solid #000",
  },
}))

export default function FormCreateInput({ getDataUpdated }) {
  const classes = useStyles()
  let hoy = new Date()
  const [messageSucces, setMessageSucces] = useState(false)
  const [messageError, setMessageError] = useState(false)
  const urlVendors = `http://apismartgo.crmsystem.tech/public/vendors/api`
  const urlProducts = `http://apismartgo.crmsystem.tech/public/products/api`
  const urlGetIdOrder = `http://apismartgo.crmsystem.tech/public/bussinessorders/api`
  const urlSaveNewBussinessOrder = `http://apismartgo.crmsystem.tech/public/bussinessorders/api`
  const [customersGet, setCustomersGet] = useState([])
  const [productsGet, setProductsGet] = useState([])
  const [detailInvoice, setDetailInvoice] = useState([])
  const [total, setTotal] = useState(0)
  const [price, setPrice] = useState(0)
  const [qty, setQty] = useState(0)
  const [diabledButton, setDiabledButton] = useState(false)
  const [getDataOrder, setGetDataOrder] = useState(true)
  const [numberOrder, setNumberOrder] = useState()
  const [state, setState] = React.useState({
    payment_method: "",
    customer: {},
  })

  //const urlSaveNewCustomer = `${process.env.GATSBY_API_URL_API_CUSTOMERS}/post`;
  const { register, handleSubmit } = useForm()
  const onSubmit = data => {
    setDiabledButton(true)
    post(
      `${urlSaveNewBussinessOrder}/productsinputs/post`,
      Object.assign(data, {
        date_input: getDate(hoy),
        id_vendor: parseInt(state?.customer?.id),
      }),
      callbackSaveOrder
    )
  }

  const callbackSaveOrder = response => {
    setDiabledButton(false)
    if (response?.data?.data === true) {
      saveDetalleFactura()
    } else {
      setMessageError(true)
      setMessageSucces(false)
    }
    setTimeout(() => {
      setMessageError(false)
      setMessageSucces(false)
    }, 20000)
  }

  const callbackResponseGetOrder = useCallback(response => {
    setGetDataOrder(false)
    setNumberOrder(parseInt(response[0]?.id) + 1)
  }, [])

  const saveDetalleFactura = () => {
    let num = 0
    let data = detailInvoice.map(item => ({
      id_product: item.id,
      id_products_input: numberOrder,
      quantity_products: parseInt(item.qty),
      cost_products: item.price,
    }))

    //for (const i  of copyDetalle) {
    //console.log(data.length);

    for (const i of data) {
      num = num + 1
      post(`${urlProducts}/productsaddinput`, i)
    }
    if (num === data.length) {
      navigate("/orders")
      setMessageSucces(true)
      setMessageError(false)
      getDataUpdated()
    }
  }

  const responseGetCustomer = response => {
    setCustomersGet(response)
  }
  const responseGetProducts = response => {
    setProductsGet(response)
  }
  const handlerSelectCustmer = value => {
    setState({ customer: value })
    setCustomersGet([])
  }

  const handlerSearchProduct = e => {
    getData(`${urlProducts}/productssku/${e.target.value}`, responseGetProducts)
    if (e.target.value === "") {
      setProductsGet([])
    }
  }

  const handlerSearchCustomer = e => {
    getData(`${urlVendors}/vendorsid/${e.target.value}`, responseGetCustomer)
    if (e.target.value === "") {
      setCustomersGet([])
    }
  }

  const handlerQuitarProducto = row => {
    const nuevoDetalle = detailInvoice.filter(
      producto => producto.id !== row.id
    )
    setDetailInvoice(nuevoDetalle)
    let subTotal = total - parseInt(row.price)
    setTotal(subTotal)
  }

  const handlerAddProductDetail = item => {
    setDetailInvoice([
      ...detailInvoice,
      {
        id: item.sku,
        name_product: item.name_product,
        qty: qty,
        price: price,
      },
    ])
  }

  useEffect(() => {
    if (getDataOrder) {
      getData(`${urlProducts}/productsinpuntid`, callbackResponseGetOrder)
    }
  }, [getDataOrder, urlGetIdOrder])

  return (
    <>
      <div className="flex flex-col md:flex-row w-full justify-between">
        <form
          className="flex flex-col w-full md:w-3/5 justify-start items-start border-2 p-2 md:p-5 border-gray-500 rounded-xl"
          onSubmit={handleSubmit(onSubmit)}
        >
          <div className="flex flex-col">
            <div className="flex flex-col justify-start items-start my-2">
              <p className="m-0 mt-2">Factura</p>
              <input
                tabindex="1"
                className="border-2 border-gray-400 rounded-md m-1  p-1"
                placeholder="Factura"
                {...register("invoice_number", {
                  maxLength: 50,
                })}
              />
            </div>
          </div>

          <div className="flex flex-col  justify-start items-start my-2">
            <p className="m-0 mt-2">Proveedor</p>
            <input
              tabindex="1"
              className="border-2 border-gray-400  rounded-md m-1  p-1"
              placeholder="Buscar..."
              onChange={e => handlerSearchCustomer(e)}
              // {...register("id_customer", { required: true, maxLength: 50 })}
            />
            {state.customer !== {} && (
              <div className="w-full">
                <p className="font-semibold text-lg text-blue-900">
                  {state.customer.business_name}
                </p>
              </div>
            )}

            <div className="w-full flex flex-col justify-start items-start">
              {customersGet &&
                customersGet.map((item, index) => (
                  <p
                    key={index}
                    className="flex flex-row justify-center items-center"
                  >
                    <Checkbox
                      onClick={() => handlerSelectCustmer(item)}
                      color="primary"
                      inputProps={{ "aria-label": "secondary checkbox" }}
                    />
                    <strong className="mr-2">
                      {item.identification_number}
                    </strong>{" "}
                    {` ${item.business_name} `}
                  </p>
                ))}
            </div>
          </div>

          <div className="w-full flex flex-col justify-start items-start my-2">
            <p className="m-0">Producto</p>
            <input
              tabindex="1"
              className="border-2 border-gray-400 rounded-md m-1 p-1"
              placeholder="Buscar..."
              onChange={e => handlerSearchProduct(e)}
              // {...register("id_customer", { required: true, maxLength: 50 })}
            />

            <div className="w-full flex flex-col justify-start items-start">
              {productsGet &&
                productsGet.map((item, index) => (
                  <div
                    key={index}
                    className="w-full flex flex-col md:flex-row justify-start items-start sm:justify-center sm:items-center md:justify-between"
                  >
                    <p className="m-0 flex text-xs flex-row justify-center items-center">
                      {` ${item.name_product} `}
                    </p>
                    <div className="my-3 sm:m-3 flex flex-row justify-end">
                      <div>
                        <input
                          className="border-2 border-gray-400 rounded-md w-32 text-xl"
                          name="price"
                          placeholder="$"
                          onChange={e => setPrice(parseInt(e.target.value))}
                        />

                        <p
                          className="text-blue-800"
                          style={{ fontSize: "10px" }}
                        >
                          Precio
                        </p>
                      </div>
                      <div>
                        <input
                          type="number"
                          className="border-2 border-gray-400 rounded-md w-20 text-xl mx-2"
                          name="qty"
                          placeholder=""
                          onChange={e => setQty(parseInt(e.target.value))}
                        />
                        <AddCircleIcon
                          className=""
                          style={{ fontSize: 30 }}
                          onClick={() => handlerAddProductDetail(item)}
                        />
                        <p
                          className="text-blue-800"
                          style={{ fontSize: "10px" }}
                        >
                          Cantidad
                        </p>
                      </div>
                    </div>
                  </div>
                ))}
            </div>
            <div className="flex flex-col">
              <div className="flex flex-wrap  md:flex-row lg:flex-row justify-start items-start my-2">
                <p className="m-0 mt-2">Total factura</p>
                <input
                  tabindex="1"
                  className="border-2 border-gray-400 rounded-md m-1  p-1"
                  placeholder="Total factura"
                  defaultValue={0}
                  {...register("total_invoice", {
                    maxLength: 50,
                  })}
                />
              </div>
            </div>
          </div>

          <div className="mx-2 my-2">
            <Button
              disabled={diabledButton}
              type="submit"
              variant="contained"
              color="primary"
            >
              Guardar
            </Button>
          </div>
        </form>
        <div className="mx-3 w-full md:w-5/12 my-3 md:m-0 md:ml-2">
          <TableContainer className="p-3" component={Paper}>
            <Table
              className={classes.table}
              size="small"
              aria-label="a dense table"
            >
              <TableHead>
                <TableRow>
                  <TableCell>Producto</TableCell>
                  <TableCell align="right">Cantidad</TableCell>
                  <TableCell align="right">Precion Unidad</TableCell>
                  <TableCell align="right">Subtotal</TableCell>
                  <TableCell align="right">Eliminar</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {detailInvoice.map(row => (
                  <TableRow key={row.name}>
                    <TableCell component="th" scope="row">
                      {row.name_product}
                    </TableCell>
                    <TableCell align="right">{row.qty}</TableCell>
                    <TableCell align="right">{row.price}</TableCell>
                    <TableCell align="right">
                      {parseInt(row.qty * row.price)}{" "}
                    </TableCell>
                    <TableCell align="right">
                      <DeleteForeverIcon
                        className=""
                        style={{ fontSize: 20 }}
                        onClick={() => handlerQuitarProducto(row)}
                      />
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </div>
      </div>
      {messageSucces && (
        <div className={classes.root}>
          <Alert severity="success">
            <AlertTitle>Excelente</AlertTitle>
            Producto creado — <strong>exitosamente!</strong>
          </Alert>
        </div>
      )}
      {messageError && (
        <div className={classes.root}>
          <Alert severity="error">
            <AlertTitle>Algo salio mal</AlertTitle>
            El producto no pudo ser creado —{" "}
            <strong>intenta nuevamente!</strong>
          </Alert>
        </div>
      )}
    </>
  )
}
