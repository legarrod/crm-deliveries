import React, { useState } from "react"
import { useForm } from "react-hook-form"
import Button from "@material-ui/core/Button"
import getDate from "../../utils/getDate"
import { makeStyles } from "@material-ui/core/styles"
import { post } from "../../api/AsyncHttpRequest"
import { Alert, AlertTitle } from "@material-ui/lab"
import ButtonUploadProducts from "./ButtonUploadProducts"
import { navigate } from "gatsby"

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    "& > * + *": {
      marginTop: theme.spacing(2),
    },
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}))

export default function FormCreateProduct() {
  const classes = useStyles()
  let hoy = new Date()
  const [messageSucces, setMessageSucces] = useState(false)
  const [messageError, setMessageError] = useState(false)
  const urlSaveNewCustomer = `http://apismartgo.crmsystem.tech/public/products/api/products/post`
  //const urlSaveNewCustomer = `${process.env.GATSBY_API_URL_API_CUSTOMERS}/post`;
  const { register, handleSubmit } = useForm()
  const [diabledButton, setDiabledButton] = useState(false)
  const onSubmit = data => {
    setDiabledButton(true)
    let newData = {
      creation_date: getDate(hoy),
      name_product: data.name_product,
      sku: data.sku,
      photo_link:
        data.photo_link ||
        "https://upload.wikimedia.org/wikipedia/commons/thumb/d/da/Imagen_no_disponible.svg/1200px-Imagen_no_disponible.svg.png",
      descriptionProd: data.descriptionProd,
    }
    post(urlSaveNewCustomer, newData, callbackSaveCustomer)
  }

  const callbackSaveCustomer = response => {
    setDiabledButton(false)
    if (response?.data?.data === true) {
      setMessageSucces(true)
      setMessageError(false)
      navigate("/orders")
    } else {
      setMessageError(true)
      setMessageSucces(false)
    }
    setTimeout(() => {
      setMessageError(false)
      setMessageSucces(false)
    }, 20000)
  }

  return (
    <>
      <ButtonUploadProducts />
      <form
        className="flex flex-wrap justify-center items-center border-2 p-5 border-gray-500 rounded-xl"
        onSubmit={handleSubmit(onSubmit)}
      >
        <div className="mx-2 my-2">
          <input
            tabindex="1"
            className="border-2 border-gray-400 w-56 rounded-md m-1 text-lg p-1"
            placeholder="Nombre"
            {...register("name_product", { required: true, maxLength: 50 })}
          />
        </div>
        <div className="mx-2 my-2">
          <input
            tabindex="2"
            className="border-2 border-gray-400  w-56 rounded-md m-1 text-lg p-1"
            placeholder="SKU"
            {...register("sku", {
              required: true,
              maxLength: 20,
            })}
          />
        </div>
        <div className="mx-2 my-2">
          <input
            tabindex="3"
            className="border-2 border-gray-400 w-56 rounded-md m-1 text-lg p-1"
            placeholder="Url imagen"
            {...register("photo_link", {
              maxLength: 200,
            })}
          />
        </div>
        <div className="mx-2 my-2">
          <input
            tabindex="4"
            className="border-2 border-gray-400 w-56 rounded-md m-1 text-lg p-1"
            placeholder="Descripcion"
            label="Descripcion"
            {...register("descriptionProd", { maxLength: 200 })}
          />
        </div>

        <div className="mx-2 my-2">
          <Button
            disabled={diabledButton}
            type="submit"
            variant="contained"
            color="primary"
          >
            Guardar
          </Button>
        </div>
      </form>

      {messageSucces && (
        <div className={classes.root}>
          <Alert severity="success">
            <AlertTitle>Excelente</AlertTitle>
            Producto creado — <strong>exitosamente!</strong>
          </Alert>
        </div>
      )}
      {messageError && (
        <div className={classes.root}>
          <Alert severity="error">
            <AlertTitle>Algo salio mal</AlertTitle>
            El producto no pudo ser creado —{" "}
            <strong>intenta nuevamente!</strong>
          </Alert>
        </div>
      )}
    </>
  )
}
