import React, { useState, useEffect, useCallback } from "react"
import { getData, put } from "../../api/AsyncHttpRequest"
import { DataGrid } from "@material-ui/data-grid"
import CreateIcon from "@material-ui/icons/Create"
import DeleteForeverIcon from "@material-ui/icons/DeleteForever"
import PhoneInTalkIcon from "@material-ui/icons/PhoneInTalk"
import { useForm } from "react-hook-form"
import getDate from "../../utils/getDate"
import Button from "@material-ui/core/Button"
import SentimentVeryDissatisfiedIcon from "@material-ui/icons/SentimentVeryDissatisfied"

export function FormUpdate({ data, setState }) {
  let hoy = new Date()
  const urlSaveNewCustomer = `http://apismartgo.crmsystem.tech/public/customer/api/customers/update`
  const { register, handleSubmit } = useForm()
  let cc = data?.data?.identification_number
  const callbackSaveCustomer = response => {
    if (response?.data?.data === true) {
      setState({
        succes: true,
        error: false,
        change: false,
      })
    } else {
      setState({
        succes: false,
        error: true,
        change: false,
      })
    }

    setTimeout(() => {
      setState({
        succes: false,
        error: false,
        change: false,
      })
    }, 20000)
  }

  const onSubmit = data => {
    put(
      urlSaveNewCustomer,
      Object.assign(data, {
        creation_date: getDate(hoy),
        last_call: getDate(hoy),
        identification_number: cc,
      }),
      callbackSaveCustomer
    )
  }

  return (
    <>
      <form
        className="flex flex-wrap justify-center items-center border-2 p-5 mx-5 mb-5 border-gray-500 rounded-xl"
        onSubmit={handleSubmit(onSubmit)}
      >
        <div className="mx-2 my-2">
          <input
            tabindex="1"
            className="border-2 border-gray-400 w-56 rounded-md m-1 text-lg p-1"
            defaultValue={data?.data?.full_name}
            placeholder="Nombre"
            {...register("full_name", { required: true, maxLength: 50 })}
          />
        </div>
        <div className="mx-2 my-2">
          <input
            tabindex="2"
            className="border-2 border-gray-400 text-gray-500 w-56 rounded-md m-1 text-lg p-1"
            defaultValue={data?.data?.document_type}
            placeholder={data?.data?.document_type}
            disabled
          />
        </div>
        <div className="mx-2 my-2">
          <input
            tabindex="3"
            className="border-2 border-gray-400 text-gray-500 w-56 rounded-md m-1 text-lg p-1"
            defaultValue={data?.data?.identification_number}
            placeholder={data?.data?.identification_number}
            disabled
          />
        </div>
        <div className="mx-2 my-2">
          <input
            tabindex="4"
            className="border-2 border-gray-400 w-56 rounded-md m-1 text-lg p-1"
            defaultValue={data?.data?.address_customer}
            placeholder="Direccion"
            {...register("address_customer", {
              required: true,
              maxLength: 80,
            })}
          />
        </div>
        <div className="mx-2 my-2">
          <input
            tabindex="5"
            className="border-2 border-gray-400 w-56 rounded-md m-1 text-lg p-1"
            defaultValue={data?.data?.email}
            placeholder="Correo"
            {...register("email", { required: true, maxLength: 50 })}
          />
        </div>
        <div className="mx-2 my-2">
          <input
            tabindex="6"
            className="border-2 border-gray-400 w-56 rounded-md m-1 text-lg p-1"
            defaultValue={data?.data?.recurrence}
            placeholder="Recurrencia"
            label="Recurrencia"
            {...register("recurrence", { required: true, maxLength: 5 })}
          />
        </div>
        <div className="mx-2 my-2">
          <input
            tabindex="6"
            className="border-2 border-gray-400 w-56 rounded-md m-1 text-lg p-1"
            defaultValue={data?.data?.cellphone}
            placeholder="Celular"
            label="Celular"
            {...register("cellphone", { required: true, maxLength: 15 })}
          />
        </div>

        <div className="mx-2 my-2">
          <Button type="submit" variant="contained" color="primary">
            Guardar
          </Button>
        </div>
      </form>
    </>
  )
}

export default function DataTable({ setState }) {
  let hoy = new Date()
  const urlAllCustomers = `http://apismartgo.crmsystem.tech/public/customer/api`
  const [rows, setRows] = useState([])
  const [buscando, setBuscando] = useState(true)

  const respponseCallback = useCallback(response => {
    setBuscando(false)
    setRows(response)
  }, [])

  const respponseCallbackEdit = response => {
    if (response.length === 1) {
      setState({ viewFormUpdate: true, data: response[0] })
      getData(`${urlAllCustomers}/customers`, respponseCallback)
    }
  }
  const respponseCallbackEditDate = response => {
    if (response) {
      setState({ succes: true })
      getData(`${urlAllCustomers}/customers`, respponseCallback)
    }
  }
  const respponseCallbackDelete = response => {
    if (response) {
      setState({ succes: true })
      getData(`${urlAllCustomers}/customers`, respponseCallback)
    }
  }

  const handlerEdit = e => {
    setState({ change: true })
    getData(`${urlAllCustomers}/customersid/${e}`, respponseCallbackEdit)
  }

  const handlerUpdate = e => {
    put(
      `${urlAllCustomers}/customers/update/last-call`,
      {
        id: e,
        last_call: getDate(hoy),
      },
      respponseCallbackEditDate
    )
  }

  const handlerDelet = e => {
    put(
      `${urlAllCustomers}/customers/delete`,
      {
        id: e,
        state_customer: 0,
      },
      respponseCallbackDelete
    )
  }
  const columns = [
    {
      field: "id",
      headerName: "",
      width: 150,
      description:
        "Que desea hacer, editar eliminar o guardar fecha de ultima llamada.",
      renderCell: params => (
        <div
          className={
            params?.row?.diference_days >= parseInt(params?.row?.recurrence) + 3
              ? "bg-red-400"
              : params?.row?.diference_days <=
                  parseInt(params?.row?.recurrence) - 2 &&
                params?.row?.diference_days <=
                  parseInt(params?.row?.recurrence) + 3
              ? null
              : "bg-yellow-400"
          }
        >
          <button
            onClick={() => handlerEdit(params.value)}
            className="m-0 mr-1"
            variant="contained"
            color="primary"
            size="small"
            style={{ marginLeft: 5 }}
          >
            <CreateIcon />
          </button>
          <button
            onClick={() => handlerDelet(params.value)}
            className="m-0 mr-1"
            variant="contained"
            color="primary"
            size="small"
            style={{ marginLeft: 5 }}
          >
            <DeleteForeverIcon />
          </button>
          <button
            onClick={() => handlerUpdate(params.value)}
            className="m-0 mr-1"
            variant="contained"
            color="primary"
            size="small"
            style={{ marginLeft: 5 }}
          >
            <PhoneInTalkIcon />
          </button>
        </div>
      ),
    },
    { field: "full_name", headerName: "Nombre completo", width: 250 },
    {
      field: "last_call",
      headerName: "Llamado el",
      description: "Fecha de la ultima llamada realizada al cliente.",
      sortable: false,
      width: 160,
    },
    {
      field: "recurrence",
      headerName: "Recurrencia",
      description: "Cada cuanto debo llamar a este cliente.",
      sortable: false,
      width: 140,
    },
    {
      field: "document_type",
      headerName: "Doc",
      description: "Tipo de documento.",
      width: 90,
    },
    {
      field: "customer_type",
      headerName: "TIP",
      description: "Tipo de cliente.",
      width: 90,
    },
    {
      field: "identification_number",
      description: "Numero de identificacion del cliente o negocio.",
      headerName: "Numero",
      width: 130,
    },
    {
      field: "address_customer",
      headerName: "Dirección",
      description: "Dirección ubicación del negocio.",
      type: "number",
      width: 200,
    },
    {
      field: "email",
      headerName: "Correo",
      description: "Correo electronico.",
      sortable: false,
      width: 160,
    },
    {
      field: "cellphone",
      headerName: "Celular",
      description: "Numero de contacto.",
      sortable: false,
      width: 130,
    },
  ]
  useEffect(() => {
    if (buscando) {
      getData(`${urlAllCustomers}/customers`, respponseCallback)
    }
  }, [urlAllCustomers, respponseCallback, buscando])

  return (
    <div style={{ height: 400, width: "100%" }}>
      {rows?.length > 0 ? (
        <DataGrid rows={rows} columns={columns} pageSize={8} />
      ) : (
        <div className="flex flex-col justify-center items-center">
          <h2 className="text-2xl text-center">
            LO SENTIMOS NO HAY INFORMACION DE CLIENTES
          </h2>
          <SentimentVeryDissatisfiedIcon style={{ fontSize: "50px" }} />{" "}
        </div>
      )}
    </div>
  )
}
