import React, { useState, useEffect } from "react"
import { useForm } from "react-hook-form"
import Button from "@material-ui/core/Button"
import getDate from "../../utils/getDate"
import FormControl from "@material-ui/core/FormControl"
import Select from "@material-ui/core/Select"
import { makeStyles } from "@material-ui/core/styles"
import InputLabel from "@material-ui/core/InputLabel"
import { post } from "../../api/AsyncHttpRequest"
import { Alert, AlertTitle } from "@material-ui/lab"
import { navigate } from "gatsby"
import ButtonUploadCustomers from "./ButtonUploadCustomers"

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    "& > * + *": {
      marginTop: theme.spacing(2),
    },
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 160,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}))

export default function App({ setStateChange }) {
  const classes = useStyles()
  let hoy = new Date()
  const [messageSucces, setMessageSucces] = useState(false)
  const [messageError, setMessageError] = useState(false)
  const urlSaveNewCustomer = `http://apismartgo.crmsystem.tech/public/customer/api/customers/post`
  //const urlSaveNewCustomer = `${process.env.GATSBY_API_URL_API_CUSTOMERS}/post`;
  const { register, handleSubmit } = useForm()
  const [state, setState] = React.useState({
    document_type: "",
    customer_type: "",
  })
  const [diabledButton, setDiabledButton] = useState(false)
  const onSubmit = data => {
    setDiabledButton(true)
    post(
      urlSaveNewCustomer,
      Object.assign(data, {
        creation_date: getDate(hoy),
        last_call: getDate(hoy),
        document_type: state.document_type,
        customer_type: state.customer_type,
      }),
      callbackSaveCustomer
    )
  }

  const callbackSaveCustomer = response => {
    setDiabledButton(false)
    console.log(response)
    if (response?.data?.data === true) {
      navigate("/orders")
      setMessageSucces(true)
      setMessageError(false)
    } else {
      setMessageError(true)
      setMessageSucces(false)
    }
    setTimeout(() => {
      setMessageError(false)
      setMessageSucces(false)
    }, 20000)
  }

  const handleChange = event => {
    const name = event.target.name
    setState({
      ...state,
      [name]: event.target.value,
    })
  }
  useEffect(() => {
    setStateChange({ viewFormUpdate: false })
  }, [setStateChange])
  return (
    <>
      <ButtonUploadCustomers />
      <form
        className="flex flex-wrap justify-center items-center border-2 p-5 border-gray-500 rounded-xl"
        onSubmit={handleSubmit(onSubmit)}
      >
        <FormControl variant="outlined" className={classes.formControl}>
          <InputLabel htmlFor="outlined-age-native-simple">
            Tipo Cliente
          </InputLabel>
          <Select
            native
            onChange={e => handleChange(e)}
            label="Tipo documento"
            inputProps={{
              name: "customer_type",
            }}
          >
            <option aria-label="None" value="" />
            <option value={"COR"}>Corporativo</option>
            <option value={"NOR"}>Normal</option>
          </Select>
        </FormControl>
        <div className="mx-2 my-2">
          <input
            tabindex="1"
            className="border-2 border-gray-400 w-56 rounded-md m-1 text-lg p-1"
            placeholder="Nombre"
            {...register("full_name", { required: true, maxLength: 50 })}
          />
        </div>
        <FormControl variant="outlined" className={classes.formControl}>
          <InputLabel htmlFor="outlined-age-native-simple">
            Tipo documento
          </InputLabel>
          <Select
            native
            onChange={e => handleChange(e)}
            label="Tipo documento"
            inputProps={{
              name: "document_type",
            }}
          >
            <option aria-label="None" value="" />
            <option value={"CE"}>Cedula Extranjeria</option>
            <option value={"CC"}>Cedula Ciudadania</option>
          </Select>
        </FormControl>

        <div className="mx-2 my-2">
          <input
            tabindex="3"
            className="border-2 border-gray-400 text-gray-500 w-56 rounded-md m-1 text-lg p-1"
            placeholder="Numero de cedula"
            {...register("identification_number", {
              maxLength: 15,
            })}
          />
        </div>
        <div className="mx-2 my-2">
          <input
            tabindex="4"
            className="border-2 border-gray-400 w-56 rounded-md m-1 text-lg p-1"
            placeholder="Direccion"
            {...register("address_customer", {
              maxLength: 80,
            })}
          />
        </div>
        <div className="mx-2 my-2">
          <input
            tabindex="5"
            className="border-2 border-gray-400 w-56 rounded-md m-1 text-lg p-1"
            placeholder="Correo"
            {...register("email", { maxLength: 50 })}
          />
        </div>
        <div className="mx-2 my-2">
          <input
            tabindex="6"
            className="border-2 border-gray-400 w-56 rounded-md m-1 text-lg p-1"
            placeholder="Celular"
            label="Celular"
            {...register("cellphone", { maxLength: 15 })}
          />
        </div>
        <div className="mx-2 my-2">
          <input
            tabindex="6"
            type="number"
            className="border-2 border-gray-400 w-56 rounded-md m-1 text-lg p-1"
            placeholder="Recurrencia"
            label="Celular"
            {...register("recurrence", { maxLength: 15 })}
          />
        </div>

        <div className="mx-2 my-2">
          <Button
            disabled={diabledButton}
            type="submit"
            variant="contained"
            color="primary"
          >
            Guardar
          </Button>
        </div>
      </form>

      {messageSucces && (
        <div className={classes.root}>
          <Alert severity="success">
            <AlertTitle>Excelente</AlertTitle>
            Cliente creado — <strong>exitosamente!</strong>
          </Alert>
        </div>
      )}
      {messageError && (
        <div className={classes.root}>
          <Alert severity="error">
            <AlertTitle>Algo salio mal</AlertTitle>
            El cliente no pudo ser creado — <strong>intenta nuevamente!</strong>
          </Alert>
        </div>
      )}
    </>
  )
}
