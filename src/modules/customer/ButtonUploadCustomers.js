import React, { useState } from "react"
import { makeStyles } from "@material-ui/core/styles"
import Button from "@material-ui/core/Button"
import CloudUploadIcon from "@material-ui/icons/CloudUpload"
import getDate from "../../utils/getDate"
import { post } from "../../api/AsyncHttpRequest"
import { navigate } from "gatsby"
import LinearProgress from "@material-ui/core/LinearProgress"
import { Alert, AlertTitle } from "@material-ui/lab"

const useStyles = makeStyles(theme => ({
  root: {
    "& > *": {
      margin: theme.spacing(1),
    },
  },
  input: {
    display: "none",
  },
}))
export default function ButtonUploadCustomers() {
  let hoy = new Date()
  const classes = useStyles()
  const urlSaveNewAllProducts = `http://apismartgo.crmsystem.tech/public/customer/api/customersall/post`
  //const [fileCsv, setFileCsv] = useState()
  const [nameCsv, setNameCsv] = useState()
  const [fileCsvTex, setFileCsvTex] = useState(null)
  const [saving, setSaving] = useState(false)
  const [messageSucces, setMessageSucces] = useState(false)
  const [messageError, setMessageError] = useState(false)

  const respponseCallback = response => {
    setSaving(false)
    if (response?.data?.data === true) {
      setMessageSucces(true)
      setMessageError(false)
      navigate("/orders")
    } else {
      setMessageError(true)
      setMessageSucces(false)
    }
    setTimeout(() => {
      setMessageError(false)
      setMessageSucces(false)
    }, 20000)
  }

  const getValues = (text, position) => {
    let todo = text.split(",")
    return todo[position]
  }
  const parseCSV = text => {
    // Obtenemos las lineas del texto
    let lines = text.replace(/\r/g, "").split("\n")
    return lines.map(line => {
      // Por cada linea obtenemos los valores
      if (line !== "") {
        let values = {
          document_type: getValues(line, 0),
          customer_type: getValues(line, 1),
          identification_number: getValues(line, 2),
          full_name: getValues(line, 3),
          address_customer: getValues(line, 4),
          email: getValues(line, 5),
          cellphone: getValues(line, 6),
          creation_date: getDate(hoy),
          last_call: getDate(hoy),
          state_customer: 1,
          recurrence: getValues(line, 7),
        }
        return values
      }
    })
  }

  const handlerSaveAll = e => {
    let file = e.target.files[0]
    setNameCsv(e.target.files[0].name)
    let reader = new FileReader()
    reader.onload = e => {
      let lines = parseCSV(e.target.result)
      lines.pop()
      setFileCsvTex(lines)
      //saveAllProducts(lines)
    }
    reader.readAsBinaryString(file)
  }

  const handleSaveAllProducts = () => {
    setSaving(true)
    post(urlSaveNewAllProducts, fileCsvTex, respponseCallback)
  }

  return (
    <div className={`${classes.root} mb-2`}>
      <p
        className="text-red-800 p-0"
        style={{ fontSize: "10px", margin: "0px" }}
      >
        * Importante: el archivo a subir no debe tener cabeceras en las columnas
      </p>
      <input
        accept=".csv"
        className={classes.input}
        id="contained-button-file"
        multiple
        type="file"
        onChange={e => handlerSaveAll(e)}
      />
      <label htmlFor="contained-button-file">
        <Button
          disabled={saving}
          htmlFor="contained-button-file"
          variant="contained"
          color="default"
          className={classes.button}
          component="span"
          startIcon={<CloudUploadIcon />}
        >
          Subir clientes
        </Button>
      </label>
      <Button
        disabled={saving}
        htmlFor="contained-button-file"
        variant="contained"
        color="primary"
        className={classes.button}
        component="span"
        onClick={handleSaveAllProducts}
      >
        Guardar
      </Button>
      <div className="font-bold mt-2" id="info">
        {nameCsv}
      </div>
      {saving && <LinearProgress />}
      {messageSucces && (
        <div className={classes.root}>
          <Alert severity="success">
            <AlertTitle>Excelente</AlertTitle>
            Producto creado — <strong>exitosamente!</strong>
          </Alert>
        </div>
      )}
      {messageError && (
        <div className={classes.root}>
          <Alert severity="error">
            <AlertTitle>Algo salio mal</AlertTitle>
            El producto no pudo ser creado —{" "}
            <strong>intenta nuevamente!</strong>
          </Alert>
        </div>
      )}
    </div>
  )
}
