import React, { useState } from "react"
import { useForm } from "react-hook-form"
import Button from "@material-ui/core/Button"
import getDate from "../../utils/getDate"
import { makeStyles } from "@material-ui/core/styles"
import { post } from "../../api/AsyncHttpRequest"
import { Alert, AlertTitle } from "@material-ui/lab"
import { navigate } from "gatsby"

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    "& > * + *": {
      marginTop: theme.spacing(2),
    },
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}))

export default function FormCreateVendor() {
  const classes = useStyles()
  let hoy = new Date()
  const [messageSucces, setMessageSucces] = useState(false)
  const [messageError, setMessageError] = useState(false)
  const urlSaveNewCustomer = `http://apismartgo.crmsystem.tech/public/vendors/api/vendors/post`
  //const urlSaveNewCustomer = `${process.env.GATSBY_API_URL_API_CUSTOMERS}/post`;
  const { register, handleSubmit } = useForm()
  const [diabledButton, setDiabledButton] = useState(false)
  const onSubmit = data => {
    setDiabledButton(true)
    post(
      urlSaveNewCustomer,
      Object.assign(data, { creation_date: getDate(hoy) }),
      callbackSaveCustomer
    )
  }

  const callbackSaveCustomer = response => {
    setDiabledButton(false)
    if (response.data.data === true) {
      setMessageSucces(true)
      setMessageError(false)
      navigate("/orders")
    } else {
      setMessageError(true)
      setMessageSucces(false)
    }
    setTimeout(() => {
      setMessageError(false)
      setMessageSucces(false)
    }, 20000)
  }

  return (
    <>
      <form
        className="flex flex-wrap justify-center items-center border-2 p-5 border-gray-500 rounded-xl"
        onSubmit={handleSubmit(onSubmit)}
      >
        <div className="mx-2 my-2">
          <input
            focus
            tabindex="1"
            className="border-2 border-gray-400 w-56 rounded-md m-1 text-lg p-1"
            placeholder="Nombre del negocios"
            {...register("business_name", { required: true, maxLength: 50 })}
          />
        </div>
        <div className="mx-2 my-2">
          <input
            tabindex="2"
            className="border-2 border-gray-400 w-56 rounded-md m-1 text-lg p-1"
            placeholder="Nombre del contacto"
            {...register("full_name", { required: true, maxLength: 50 })}
          />
        </div>
        <div className="mx-2 my-2">
          <input
            tabindex="3"
            className="border-2 border-gray-400 w-56 rounded-md m-1 text-lg p-1"
            placeholder="Nit"
            {...register("nit", { required: true, maxLength: 15 })}
          />
        </div>
        <div className="mx-2 my-2">
          <input
            tabindex="4"
            className="border-2 border-gray-400 w-56 rounded-md m-1 text-lg p-1"
            placeholder="Direccion"
            {...register("business_address", {
              required: true,
              maxLength: 80,
            })}
          />
        </div>
        <div className="mx-2 my-2">
          <input
            tabindex="5"
            className="border-2 border-gray-400 w-56 rounded-md m-1 text-lg p-1"
            placeholder="Celular"
            label="Celular"
            {...register("business_phone", { required: true, maxLength: 15 })}
          />
        </div>
        <div className="mx-2 my-2">
          <input
            tabindex="6"
            className="border-2 border-gray-400 w-56 rounded-md m-1 text-lg p-1"
            placeholder="Correo"
            {...register("business_email", { required: true, maxLength: 50 })}
          />
        </div>

        <div className="mx-2 my-2">
          <Button
            disabled={diabledButton}
            type="submit"
            variant="contained"
            color="primary"
          >
            Guardar
          </Button>
        </div>
      </form>

      {messageSucces && (
        <div className={classes.root}>
          <Alert severity="success">
            <AlertTitle>Excelente</AlertTitle>
            Proveedor creado — <strong>exitosamente!</strong>
          </Alert>
        </div>
      )}
      {messageError && (
        <div className={classes.root}>
          <Alert severity="error">
            <AlertTitle>Algo salio mal</AlertTitle>
            El proveedor no pudo ser creado —{" "}
            <strong>intenta nuevamente!</strong>
          </Alert>
        </div>
      )}
    </>
  )
}
