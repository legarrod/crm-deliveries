import React, { useState, useEffect, useCallback } from "react"
import { getData, put } from "../../api/AsyncHttpRequest"
import { DataGrid } from "@material-ui/data-grid"
import CreateIcon from "@material-ui/icons/Create"
import DeleteForeverIcon from "@material-ui/icons/DeleteForever"
import { useForm } from "react-hook-form"
import getDate from "../../utils/getDate"
import Button from "@material-ui/core/Button"
import SentimentVeryDissatisfiedIcon from "@material-ui/icons/SentimentVeryDissatisfied"

export function FormUpdate({ data, setState }) {
  let hoy = new Date()
  const urlSaveNewCustomer = `http://apismartgo.crmsystem.tech/public/vendors/api/vendors/update`
  const { register, handleSubmit } = useForm()
  let nit = data?.data?.nit
  const callbackSaveCustomer = useCallback(
    response => {
      if (response?.data?.data === true) {
        setState({
          succes: true,
          error: false,
          change: false,
        })
      } else {
        setState({
          succes: false,
          error: true,
          change: false,
        })
      }

      setTimeout(() => {
        setState({
          succes: false,
          error: false,
          change: false,
        })
      }, 20000)
    },
    [setState]
  )

  const onSubmit = data => {
    put(
      urlSaveNewCustomer,
      Object.assign(data, {
        creation_date: getDate(hoy),
        nit: nit,
      }),
      callbackSaveCustomer
    )
  }

  return (
    <>
      <form
        className="flex flex-wrap m-5 justify-center items-center border-2 p-5 border-gray-500 rounded-xl"
        onSubmit={handleSubmit(onSubmit)}
      >
        <div className="mx-2 my-2">
          <input
            tabindex="0"
            className="border-2 border-gray-400 w-56 rounded-md m-1 text-lg p-1"
            defaultValue={data?.data?.business_name}
            placeholder="Nombre del negocio"
            {...register("business_name", { required: true, maxLength: 50 })}
          />
        </div>
        <div className="mx-2 my-2">
          <input
            tabindex="-1"
            className="border-2 border-gray-400 w-56 rounded-md m-1 text-lg p-1"
            defaultValue={data?.data?.full_name}
            placeholder="Nombre del contacto"
            {...register("full_name", { required: true, maxLength: 50 })}
          />
        </div>
        <div className="mx-2 my-2">
          <input
            tabindex="-2"
            className="border-2 border-gray-400 text-gray-500 w-56 rounded-md m-1 text-lg p-1"
            defaultValue={data?.data?.nit}
            placeholder={data?.data?.nit}
            disabled
          />
        </div>
        <div className="mx-2 my-2">
          <input
            tabindex="-3"
            className="border-2 border-gray-400 w-56 rounded-md m-1 text-lg p-1"
            defaultValue={data?.data?.business_address}
            placeholder="Direccion"
            {...register("business_address", {
              required: true,
              maxLength: 80,
            })}
          />
        </div>
        <div className="mx-2 my-2">
          <input
            tabindex="-4"
            className="border-2 border-gray-400 w-56 rounded-md m-1 text-lg p-1"
            defaultValue={data?.data?.business_phone}
            placeholder="Celular"
            label="Celular"
            {...register("business_phone", { required: true, maxLength: 15 })}
          />
        </div>
        <div className="mx-2 my-2">
          <input
            tabindex="-5"
            className="border-2 border-gray-400 w-56 rounded-md m-1 text-lg p-1"
            defaultValue={data?.data?.business_email}
            placeholder="Correo"
            {...register("business_email", { maxLength: 50 })}
          />
        </div>

        <div className="mx-2 my-2">
          <Button type="submit" variant="contained" color="primary">
            Guardar
          </Button>
        </div>
      </form>
    </>
  )
}

export default function DataTable({ setState }) {
  const urlAllCustomers = `http://apismartgo.crmsystem.tech/public/vendors/api`
  const [rows, setRows] = useState([])
  const [buscando, setBuscando] = useState(true)

  const respponseCallback = useCallback(response => {
    setBuscando(false)
    setRows(response)
  }, [])
  const respponseCallbackEdit = response => {
    if (response.length === 1) {
      setState({ viewFormUpdate: true, data: response[0] })
      getData(`${urlAllCustomers}/vendors`, respponseCallback)
    }
  }
  const respponseCallbackDelete = response => {
    if (response) {
      setState({ succes: true })
      getData(`${urlAllCustomers}/vendors`, respponseCallback)
    }
  }

  const handlerEdit = e => {
    setState({ viewFormUpdate: true })
    getData(`${urlAllCustomers}/vendorsid/${e}`, respponseCallbackEdit)
  }

  const handlerDelet = e => {
    put(
      `${urlAllCustomers}/vendors/delete`,
      {
        id: e,
        state_vendors: 0,
      },
      respponseCallbackDelete
    )
  }

  const columns = [
    {
      field: "id",
      headerName: "",
      width: 150,
      description:
        "Que desea hacer, editar eliminar o guardar fecha de ultima llamada.",
      renderCell: params => (
        <div>
          <button
            onClick={() => handlerEdit(params.row.nit)}
            className="m-0 mr-1"
            variant="contained"
            color="primary"
            size="small"
            style={{ marginLeft: 5 }}
          >
            <CreateIcon />
          </button>
          <button
            onClick={() => handlerDelet(params.value)}
            className="m-0 mr-1"
            variant="contained"
            color="primary"
            size="small"
            style={{ marginLeft: 5 }}
          >
            <DeleteForeverIcon />
          </button>
        </div>
      ),
    },
    {
      field: "business_name",
      headerName: "Negocio",
      width: 300,
    },
    { field: "full_name", headerName: "Contacto", width: 160 },
    { field: "nit", headerName: "Nit", width: 140 },
    {
      field: "business_address",
      headerName: "Direccion",
      type: "number",
      width: 200,
    },
    {
      field: "business_phone",
      headerName: "Telefono",
      description: "This column has a value getter and is not sortable.",
      sortable: false,
      width: 140,
    },
    {
      field: "business_email",
      headerName: "Correo",
      description: "This column has a value getter and is not sortable.",
      sortable: false,
      width: 200,
    },
  ]

  useEffect(() => {
    if (buscando) {
      getData(`${urlAllCustomers}/vendors`, respponseCallback)
    }
  }, [urlAllCustomers, respponseCallback, buscando])

  return (
    <div style={{ height: 400, width: "100%" }}>
      {rows?.length > 0 ? (
        <DataGrid rows={rows} columns={columns} pageSize={5} />
      ) : (
        <div className="flex flex-col justify-center items-center">
          <h2 className="text-2xl text-center">
            LO SENTIMOS NO HAY INFORMACION DE PROVEEDORES
          </h2>
          <SentimentVeryDissatisfiedIcon style={{ fontSize: "50px" }} />{" "}
        </div>
      )}
    </div>
  )
}
