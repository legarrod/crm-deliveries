import React from "react"
import PropTypes from "prop-types"
import { makeStyles } from "@material-ui/core/styles"
import AppBar from "@material-ui/core/AppBar"
import Tabs from "@material-ui/core/Tabs"
import Tab from "@material-ui/core/Tab"
import Typography from "@material-ui/core/Typography"
import Box from "@material-ui/core/Box"
import FormCreateVendor from "./FormCreateVendor"
import ListVendors, { FormUpdate } from "./ListVendors"
import { Alert, AlertTitle } from "@material-ui/lab"

function TabPanel(props) {
  const { children, value, index, ...other } = props

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  )
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
}

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
  estiloTabs: {
    backgroundColor: "#000000",
    color: "#59D741",
  },
}))

export default function Vendors() {
  const classes = useStyles()
  const [value, setValue] = React.useState(0)
  const [state, setState] = React.useState({
    viewFormUpdate: false,
    data: {},
    succes: false,
    error: false,
    change: false,
  })
  function a11yProps(index) {
    return {
      id: `simple-tab-${index}`,
      "aria-controls": `simple-tabpanel-${index}`,
    }
  }
  const handleChange = (event, newValue) => {
    setValue(newValue)
  }
  return (
    <div>
      <div className={`mt-8 px-2 sm:px-9 ${classes.root}`}>
        <AppBar
          position="static"
          classes={{
            root: classes.estiloTabs,
          }}
        >
          <Tabs
            value={value}
            onChange={handleChange}
            aria-label="simple tabs example"
          >
            <Tab label="Ver proveedores" {...a11yProps(0)} />
            <Tab
              label="Crear proveedor"
              {...a11yProps(1)}
              onClick={() => setState({ viewFormUpdate: false })}
            />
          </Tabs>
        </AppBar>
        <TabPanel value={value} index={0}>
          <ListVendors setState={setState} />
        </TabPanel>
        <TabPanel value={value} index={1}>
          <FormCreateVendor />
        </TabPanel>
      </div>
      {state.viewFormUpdate && <FormUpdate data={state} setState={setState} />}
      {state.succes && (
        <div className={classes.root}>
          <Alert severity="success">
            <AlertTitle>Excelente</AlertTitle>
            Información actualizada — <strong>exitosamente!</strong>
          </Alert>
        </div>
      )}
      {state.error && (
        <div className={classes.root}>
          <Alert severity="error">
            <AlertTitle>Algo salio mal</AlertTitle>
            La información no se pudo actualizar —{" "}
            <strong>intenta nuevamente!</strong>
          </Alert>
        </div>
      )}
    </div>
  )
}
