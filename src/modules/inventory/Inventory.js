import React, { useState } from "react"
import { getData, put, post } from "../../api/AsyncHttpRequest"
import SaveIcon from "@material-ui/icons/Save"
import Button from "@material-ui/core/Button"
import ButtonUploadInputs from "./ButtonUploadInputs"
import { Alert, AlertTitle } from "@material-ui/lab"

export default function Inventory() {
  const [price, setPrice] = useState(0)
  const [qty, setQty] = useState(0)
  const urlProducts = `http://apismartgo.crmsystem.tech/public/products/api`
  const [productsGet, setProductsGet] = useState([])
  const [productsGetAll, setProductsGetAll] = useState([])
  const [initialPrice, setInitialPrice] = useState(0)
  const [initialQty, setInitialQty] = useState(0)
  const [messageSucces, setMessageSucces] = useState(false)
  const [messageError, setMessageError] = useState(false)

  const responseGetProducts = response => {
    setProductsGet(response)
  }
  const responseGetProductsALL = response => {
    setProductsGetAll(response)
  }

  const callbackUpdateInput = response => {
    if (response?.data?.data === true) {
      setProductsGetAll([])
      setProductsGet([])
      setMessageError(false)
      setMessageSucces(true)
    } else {
      setMessageError(true)
      setMessageSucces(false)
    }
    setTimeout(() => {
      setMessageError(false)
      setMessageSucces(false)
    }, 20000)
  }

  const handlerSearchProduct = e => {
    getData(
      `${urlProducts}/productsinput/${e.target.value}`,
      responseGetProducts
    )
    if (e.target.value === "") {
      setProductsGet([])
    }
  }

  const handlerSearchProductAll = e => {
    getData(
      `${urlProducts}/productsskuadd/${e.target.value}`,
      responseGetProductsALL
    )
    if (e.target.value === "") {
      setProductsGetAll([])
    }
  }

  const handlerReset = () => {
    setProductsGet([])
    setProductsGetAll([])
  }

  const handlerUpdateProductInventory = item => {
    let params = {
      cantidad: parseInt(qty),
      costo: parseInt(price),
      id: item.sku,
    }
    put(`${urlProducts}/productsupdateinput`, params, callbackUpdateInput)
  }

  const handlerAddProductInventory = item => {
    let params = {
      id_products_input: 1,
      cantidad: parseInt(initialQty),
      costo: parseInt(initialPrice),
      id_product: item.sku,
    }

    post(`${urlProducts}/productsaddinput`, params, callbackUpdateInput)
  }
  return (
    <>
      <ButtonUploadInputs />
      <div className="grid grid-cols-1 lg:grid-cols-2">
        <div className="flex flex-col justify-start items-start my-2 border-2 rounded-2xl border-gray-800 p-2 mx-2">
          <p className="w-full m-0 my-2 text-xl text-center">
            Seccion para actualizar costo y cantidad a un producto
          </p>
          <div className="w-full flex flex-wrap justify-between">
            <p className="m-0 mt-2 font-semibold text-2xl">Producto</p>
            <input
              tabindex="1"
              className="border-2 border-gray-400 rounded-md m-1 p-1"
              placeholder="Buscar..."
              onChange={e => handlerSearchProduct(e)}
              // {...register("id_customer", { required: true, maxLength: 50 })}
            />
            <Button
              htmlFor="contained-button-file"
              variant="contained"
              color="primary"
              component="span"
              onClick={handlerReset}
            >
              BORRAR
            </Button>
          </div>

          <div className="w-full flex flex-col  items-start">
            {productsGet &&
              productsGet.map((item, index) => (
                <div
                  key={index}
                  className="w-full flex flex-wrap md:flex-row justify-between items-center"
                >
                  <p className="m-0 normal-case text-sm flex flex-row justify-center items-center">
                    {`${item.sku} | ${item.producto} `}
                  </p>
                  <div className="flex flex-wrap items-center">
                    <div className="my-3 sm:m-3">
                      <input
                        className="border-2 border-gray-400 rounded-md w-32  text-base"
                        name="price"
                        placeholder="COSTO"
                        onChange={e => setPrice(e.target.value)}
                        defaultValue={`${item.costo}`}
                      />
                      <p className="text-blue-800" style={{ fontSize: "10px" }}>
                        Costo del producto
                      </p>
                    </div>
                    <div className="my-3 sm:m-3">
                      <input
                        className="border-2 border-gray-400 rounded-md w-20 text-base"
                        name="qty"
                        placeholder="CANT."
                        onChange={e => setQty(e.target.value)}
                        defaultValue={` ${item.cantidad} `}
                      />
                      <SaveIcon
                        className=""
                        style={{ fontSize: 30 }}
                        onClick={() => handlerUpdateProductInventory(item)}
                      />
                      <p className="text-blue-800" style={{ fontSize: "10px" }}>
                        Cantidad del producto
                      </p>
                    </div>
                  </div>
                </div>
              ))}
          </div>
        </div>
        <div className="flex flex-col justify-start items-start my-2 border-2 rounded-2xl border-gray-800 p-2 mx-2">
          <p className="w-full m-0 my-2 text-xl text-center">
            Seccion para agregar costo y cantidad a un producto
          </p>
          <p
            className="w-full text-red-800 p-0 text-center"
            style={{ fontSize: "10px", margin: "0px" }}
          >
            * Solo se puede buscar por SKU
          </p>
          <div className="w-full flex flex-wrap justify-between">
            <p className="m-0 mt-2 font-semibold text-2xl">Producto</p>
            <input
              tabindex="1"
              className="border-2 border-gray-400 rounded-md m-1 p-1"
              placeholder="Buscar..."
              onChange={e => handlerSearchProductAll(e)}
              // {...register("id_customer", { required: true, maxLength: 50 })}
            />
            <Button
              htmlFor="contained-button-file"
              variant="contained"
              color="primary"
              component="span"
              onClick={handlerReset}
            >
              BORRAR
            </Button>
          </div>

          <div className="w-full flex flex-col  items-start">
            {productsGetAll &&
              productsGetAll.map((item, index) => (
                <div
                  key={index}
                  className="w-full flex flex-wrap md:flex-row justify-between items-center"
                >
                  <p className="m-0 normal-case text-sm flex flex-row justify-center items-center">
                    {`${item.sku} | ${item.name_product} `}
                  </p>

                  <div className="flex flex-wrap items-center">
                    <div className="my-3 sm:m-3">
                      <input
                        className="border-2 border-gray-400 rounded-md w-32 text-base"
                        name="price"
                        placeholder="COSTO"
                        onChange={e => setInitialPrice(e.target.value)}
                      />
                      <p className="text-blue-800" style={{ fontSize: "10px" }}>
                        Costo del producto
                      </p>
                    </div>
                    <div className="my-3 sm:m-3">
                      <input
                        className="border-2 border-gray-400 rounded-md w-20 text-base"
                        name="qty"
                        placeholder="CANT."
                        onChange={e => setInitialQty(e.target.value)}
                      />
                      <SaveIcon
                        className=""
                        style={{ fontSize: 30 }}
                        onClick={() => handlerAddProductInventory(item)}
                      />
                      <p className="text-blue-800" style={{ fontSize: "10px" }}>
                        Cantidad del producto
                      </p>
                    </div>
                  </div>
                </div>
              ))}
          </div>
        </div>
      </div>
      {messageSucces && (
        <div>
          <Alert severity="success">
            <AlertTitle>Excelente</AlertTitle>
            Operacion — <strong>exitosa!</strong>
          </Alert>
        </div>
      )}
      {messageError && (
        <div>
          <Alert severity="error">
            <AlertTitle>Algo salio mal</AlertTitle>
            No sabemos que paso — <strong>intenta nuevamente!</strong>
          </Alert>
        </div>
      )}
    </>
  )
}
