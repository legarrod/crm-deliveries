export const calculatefinalcost = (customType, coste) => {
  let result = 0
  const typeCustomer = (type, value) => {
    if (type === value) {
      return true
    }
    return false
  }

  if (typeCustomer(customType, "COR")) {
    result = coste * 0.1 + coste
  }
  if (typeCustomer(customType, "NOR")) {
    if (coste > 0 && coste <= 10000) {
      result = coste * 0.3 + coste
    }
    if (coste >= 10001 && coste <= 20000) {
      result = coste * 0.25 + coste
    }
    if (coste >= 20001 && coste <= 30000) {
      result = coste * 0.2 + coste
    }
    if (coste >= 30001 && coste <= 100000) {
      result = coste * 0.15 + coste
    }
    if (coste >= 100001) {
      result = coste * 0.1 + coste
    }
  }
  return result
}
