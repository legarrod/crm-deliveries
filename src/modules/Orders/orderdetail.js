import React, { useEffect, useState } from "react"
import { getData, put } from "../../api/AsyncHttpRequest"
import ProductsDetail from "./ProductsDetail"
import IconButton from "@material-ui/core/IconButton"
import HighlightOffIcon from "@material-ui/icons/HighlightOff"
import DoneAllIcon from "@material-ui/icons/DoneAll"
import getDate from "../../utils/getDate"
import Button from "@material-ui/core/Button"
import { makeStyles } from "@material-ui/core/styles"
const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
}))
function OrderDetail({
  viewDetails,
  setViewDetails,
  setMessageError,
  setMessageSucces,
  setControlRefreshData,
}) {
  let hoy = new Date()
  const classes = useStyles()
  const urlAllCustomers = `http://apismartgo.crmsystem.tech/public/bussinessorders/api`
  const [dataTable, setDataTable] = useState([])
  const [observatiosn, setObservatiosn] = useState("")
  const [geoLocationSave, setGeoLocationSave] = useState({
    latitude_coordinate: "",
    altitude_coordinate: "",
  })
  navigator.geolocation.getCurrentPosition(function (position) {
    setGeoLocationSave({
      latitude_coordinate: position.coords.latitude,
      altitude_coordinate: position.coords.longitude,
    })
  })
  const [diabledButton, setDiabledButton] = useState(false)
  const respponseCallback = response => {
    setDataTable(response)
  }
  const callbackResponseSave = response => {
    if (response?.data?.data === true) {
      setMessageSucces(true)
      setMessageError(false)
      setDiabledButton(false)
      setViewDetails({ view: !viewDetails.view })
    } else {
      setDiabledButton(true)
      setMessageError(true)
      setMessageSucces(false)
    }
  }

  const handlerUpdateOrder = () => {
    setControlRefreshData(true)
    setDiabledButton(true)
    let dataSave = {
      id: parseInt(viewDetails.data.idorder),
      delivery_date: getDate(hoy),
      delivery_observation: observatiosn,
      orders_status: 2,
      latitude_coordinate: geoLocationSave.latitude_coordinate,
      altitude_coordinate: geoLocationSave.altitude_coordinate,
    }
    put(
      `${urlAllCustomers}/bussinessorders/update`,
      dataSave,
      callbackResponseSave
    )
  }

  useEffect(() => {
    let id = parseInt(viewDetails.data.idorder)
    getData(`${urlAllCustomers}/bussinessordersid/${id}`, respponseCallback)
  }, [urlAllCustomers, viewDetails])
  return (
    <div className="w-full sm:w-4/5 md:w-2/5 border-2 mt-5 rounded-2xl border-blue-500 p-4">
      <div className="flex flex-col items-start ">
        <div className="w-full flex justify-end">
          <IconButton
            color="secondary"
            aria-label="add an alarm"
            onClick={() => setViewDetails({ view: !viewDetails.view })}
          >
            <HighlightOffIcon />
          </IconButton>
        </div>
        <div className="flex flex-wrap md:flex-row justify-start items-end">
          <h3 className="m-0 text-xl font-semibold">Nombre del cliente: </h3>
          <p className="m-0 ml-3 text-base">{viewDetails.data.name}</p>
        </div>
        <div className="flex flex-wrap md:flex-row justify-start items-end">
          <h3 className="m-0 text-xl font-semibold">Direccion de entrega: </h3>
          <p className="m-0 ml-3 text-base">{viewDetails.data.address_cus}</p>
        </div>
        <div className="flex flex-wrap md:flex-row justify-start items-end">
          <h3 className="m-0 text-xl font-semibold">Telefono de cliente: </h3>
          <p className="m-0 ml-3 text-base">{viewDetails.data.phone}</p>
        </div>
      </div>

      <ProductsDetail dataTable={dataTable} />
      <div className="flex flex-wrap sm:flex-row md:flex-row lg:flex-row justify-start items-center my-2">
        <p className="m-0">Observaciones</p>

        <textarea
          className="border-b-4 border-2 w-full border-gray-400 rounded-md m-0 my-3 px-2 h-16 shadow-lg"
          name="observations"
          placeholder="Observaciones"
          onChange={e => setObservatiosn(e.target.value)}
          // {...register("observations", { maxLength: 200 })}
        />
      </div>
      <Button
        disabled={diabledButton}
        onClick={() => handlerUpdateOrder()}
        variant="contained"
        color="primary"
        className={classes.button}
        startIcon={<DoneAllIcon />}
      >
        ENTREGAR
      </Button>
    </div>
  )
}

export default OrderDetail
