import React, { useEffect, useState, useCallback } from "react"
import { makeStyles } from "@material-ui/core/styles"
import CardOrders from "./CardOrders"
import { getData } from "../../api/AsyncHttpRequest"
import OrderDetail from "./orderdetail"
import { Alert, AlertTitle } from "@material-ui/lab"

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
}))

export default function Deliver() {
  const classes = useStyles()
  const urlAllCustomers = `http://apismartgo.crmsystem.tech/public/bussinessorders/api`
  const [rows, setRows] = useState([])
  const [buscando, setBuscando] = useState(true)
  const [messageSucces, setMessageSucces] = useState(false)
  const [messageError, setMessageError] = useState(false)
  const [controlRefreshData, setControlRefreshData] = useState(false)
  const [viewDetails, setViewDetails] = useState({
    view: false,
    data: "",
  })
  const respponseCallback = useCallback(response => {
    setControlRefreshData(false)
    setBuscando(false)
    setRows(response)
  }, [])

  const getDataUpdated = () => {
    getData(`${urlAllCustomers}/bussinessorders`, respponseCallback)
  }

  useEffect(() => {
    if (buscando) {
      getDataUpdated()
    }
    controlRefreshData && getDataUpdated()
  }, [buscando, controlRefreshData])

  return (
    <div>
      <div className="px-2 sm:px-9 pt-5">
        {!viewDetails.view ? (
          <div
            className={`mt-5 w-full flex flex-wrap justify-center items-center ${classes.root}`}
          >
            {" "}
            {rows.length >= 1 && <CardOrders setViewDetails={setViewDetails} />}
          </div>
        ) : (
          <div className="flex w-full justify-center items-center mb-5">
            <OrderDetail
              setControlRefreshData={setControlRefreshData}
              setMessageSucces={setMessageSucces}
              setMessageError={setMessageError}
              viewDetails={viewDetails}
              getDataUpdated={getDataUpdated}
              setViewDetails={setViewDetails}
            />
          </div>
        )}
        {messageSucces && (
          <div className={classes.root}>
            <Alert severity="success">
              <AlertTitle>Excelente</AlertTitle>
              Orden entregada — <strong>exitosamente!</strong>
            </Alert>
          </div>
        )}
        {messageError && (
          <div className={classes.root}>
            <Alert severity="error">
              <AlertTitle>Algo salio mal</AlertTitle>
              La orden no se pudo guardar — <strong>intenta nuevamente!</strong>
            </Alert>
          </div>
        )}
      </div>
    </div>
  )
}
