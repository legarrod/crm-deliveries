import React, { useEffect, useState, useCallback } from "react"
import { makeStyles } from "@material-ui/core/styles"
import Sorting from "./Sorting"
import { getData } from "../../api/AsyncHttpRequest"
import OrderDetail from "./orderdetail"
import FormOrders from "./FormOrders"
import { Alert, AlertTitle } from "@material-ui/lab"

const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
}))

export default function Orders() {
  const classes = useStyles()
  const urlAllCustomers = `http://apismartgo.crmsystem.tech/public/bussinessorders/api`
  const [rows, setRows] = useState([])
  const [buscando, setBuscando] = useState(true)
  const [messageSucces, setMessageSucces] = useState(false)
  const [messageError, setMessageError] = useState(false)
  const [controlRefreshData, setControlRefreshData] = useState(false)
  const [viewDetails, setViewDetails] = useState({
    view: false,
    data: "",
  })

  const respponseCallback = useCallback(response => {
    setControlRefreshData(false)
    setBuscando(false)
    setRows(response)
  }, [])

  const getDataUpdated = (urlAllCustomers, respponseCallback) => {
    getData(`${urlAllCustomers}/bussinessorders`, respponseCallback)
  }

  useEffect(() => {
    if (buscando) {
      getDataUpdated(urlAllCustomers, respponseCallback)
    }
    controlRefreshData && getDataUpdated(urlAllCustomers, respponseCallback)
  }, [urlAllCustomers, respponseCallback, buscando, controlRefreshData])
  return (
    <div className="grid grid-cols-1 md:grid-cols-2 h-auto mb-9">
      <div>
        <FormOrders
          getDataUpdated={getDataUpdated}
          respponseCallback={respponseCallback}
          urlAllCustomers={urlAllCustomers}
        />
        {messageSucces && (
          <div className={`${classes.root} w-full`}>
            <Alert severity="success">
              <AlertTitle>Excelente</AlertTitle>
              Orden entregada — <strong>exitosamente!</strong>
            </Alert>
          </div>
        )}
        {messageError && (
          <div className={classes.root}>
            <Alert severity="error">
              <AlertTitle>Algo salio mal</AlertTitle>
              La orden no se pudo guardar — <strong>intenta nuevamente!</strong>
            </Alert>
          </div>
        )}
      </div>

      {!viewDetails.view ? (
        <div
          className={`w-full flex justify-center items-center ${classes.root}`}
        >
          {rows.length >= 1 && (
            <div>
              <p className="w-full mt-12 text-center my-2">
                Ordenar pedidos segun ruta
              </p>
              <Sorting setViewDetails={setViewDetails} />
            </div>
          )}
        </div>
      ) : (
        <div className="flex w-full justify-center items-center mb-5">
          <OrderDetail
            setControlRefreshData={setControlRefreshData}
            setMessageSucces={setMessageSucces}
            setMessageError={setMessageError}
            viewDetails={viewDetails}
            getDataUpdated={getDataUpdated}
            setViewDetails={setViewDetails}
          />
        </div>
      )}
    </div>
  )
}
