import React, { useState, useEffect } from "react"
import { useForm } from "react-hook-form"
import Button from "@material-ui/core/Button"
import getDate from "../../utils/getDate"
import { makeStyles } from "@material-ui/core/styles"
import { post, getData } from "../../api/AsyncHttpRequest"
import { Alert, AlertTitle } from "@material-ui/lab"
import InputLabel from "@material-ui/core/InputLabel"
import FormControl from "@material-ui/core/FormControl"
import Select from "@material-ui/core/Select"
import Checkbox from "@material-ui/core/Checkbox"
import AddCircleIcon from "@material-ui/icons/AddCircle"
import Table from "@material-ui/core/Table"
import TableBody from "@material-ui/core/TableBody"
import TableCell from "@material-ui/core/TableCell"
import TableContainer from "@material-ui/core/TableContainer"
import TableHead from "@material-ui/core/TableHead"
import TableRow from "@material-ui/core/TableRow"
import Paper from "@material-ui/core/Paper"
import DeleteForeverIcon from "@material-ui/icons/DeleteForever"
import { calculatefinalcost } from "./utilsPrices"
import { navigate } from "gatsby"

const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    "& > * + *": {
      marginTop: theme.spacing(2),
    },
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 200,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
  paper: {
    position: "absolute",
    width: 400,
    backgroundColor: "#ffffff",
    border: "2px solid #000",
  },
}))

export default function FormOrders({
  setViewCreateOrder,
  getDataUpdated,
  urlAllCustomers,
  respponseCallback,
}) {
  const classes = useStyles()
  let hoy = new Date()
  const [messageSucces, setMessageSucces] = useState(false)
  const [messageError, setMessageError] = useState(false)
  const urlCustomer = `http://apismartgo.crmsystem.tech/public/customer/api`
  const urlProducts = `http://apismartgo.crmsystem.tech/public/products/api`
  const urlGetIdOrder = `http://apismartgo.crmsystem.tech/public/bussinessorders/api`
  const urlSaveNewBussinessOrder = `http://apismartgo.crmsystem.tech/public/bussinessorders/api`
  const [customersGet, setCustomersGet] = useState([])
  const [productsGet, setProductsGet] = useState([])
  const [detailInvoice, setDetailInvoice] = useState([])
  const [total, setTotal] = useState(0)
  const [qty, setQty] = useState(0)
  const [diabledButton, setDiabledButton] = useState(false)
  const [getDataOrder, setGetDataOrder] = useState(true)
  const [numberOrder, setNumberOrder] = useState()
  const [customerType, setCustomerType] = useState("")
  const [viewSearchProduct, setViewSearchProduct] = useState(false)
  const [state, setState] = React.useState({
    payment_method: "",
    customer: {},
  })

  //const urlSaveNewCustomer = `${process.env.GATSBY_API_URL_API_CUSTOMERS}/post`;
  const { register, handleSubmit } = useForm()
  const [errorCustomerReque, setErrorCustomerReque] = useState(false)
  const [errorDetailMayior, setErrorDetailMayior] = useState(false)
  const onSubmit = data => {
    if (state?.customer?.id) {
      setErrorCustomerReque(false)
      if (detailInvoice.length > 0) {
        setDiabledButton(true)
        post(
          `${urlSaveNewBussinessOrder}/bussinessorders/post`,
          Object.assign(data, {
            order_date: getDate(hoy),
            id_employee: 1,
            order_number: numberOrder + 1,
            id_customer: parseInt(state?.customer?.id),
            payment_method: state?.payment_method,
          }),
          callbackSaveOrder
        )
      } else {
        setErrorDetailMayior(true)
      }
    } else {
      setErrorCustomerReque(true)
    }
  }

  const callbackSaveOrder = response => {
    setDiabledButton(false)
    if (response?.data?.data === true) {
      getData(
        `${urlGetIdOrder}/bussinessordersforids/${numberOrder + 1}`,
        callbackGetOrder
      )
    } else {
      setMessageError(true)
      setMessageSucces(false)
    }
    setTimeout(() => {
      setMessageError(false)
      setMessageSucces(false)
    }, 20000)
  }

  const callbackGetOrder = response => {
    if (response.length >= 0) {
      saveDetalleFactura(response[0]?.id_order)
    }
  }

  const callbackResponseGetOrder = response => {
    setGetDataOrder(false)
    setNumberOrder(
      response.length === 0 ? 1 : parseInt(response[0]?.id_order) + 1
    )
  }

  const saveDetalleFactura = id => {
    let num = 0
    let data = detailInvoice.map(item => ({
      id_bussiness_order: parseInt(id),
      id_product: item?.id,
      quantity_products: parseInt(item.qty),
      status_product: 1,
      price_product: parseInt(item.price),
      subtotal: item.qty * item.price,
    }))
    //for (const i  of copyDetalle) {
    //console.log(data.length);
    for (const i of data) {
      num = num + 1
      post(`${urlSaveNewBussinessOrder}/bussinessordersdetails/post`, i)
    }
    if (num === data.length) {
      setErrorDetailMayior(false)
      setErrorCustomerReque(false)
      navigate("/orders")
      setViewSearchProduct(false)
      setMessageSucces(true)
      setMessageError(false)
      getDataUpdated(urlAllCustomers, respponseCallback)
      setViewCreateOrder(false)
    }
  }

  const responseGetCustomer = response => {
    setCustomersGet(response)
  }
  const responseGetProducts = response => {
    setProductsGet(response)
  }
  const handlerSelectCustmer = value => {
    setErrorCustomerReque(false)
    setViewSearchProduct(true)
    setCustomerType(value.customer_type)
    setState({ customer: value })
    setCustomersGet([])
  }

  const handlerSearchProduct = e => {
    getData(
      `${urlProducts}/productsinput/${e.target.value}`,
      responseGetProducts
    )
    if (e.target.value === "") {
      setProductsGet([])
    }
  }

  const handlerSearchCustomer = e => {
    getData(`${urlCustomer}/customers/${e.target.value}`, responseGetCustomer)
    if (e.target.value === "") {
      setCustomersGet([])
    }
  }

  const handlerQuitarProducto = row => {
    const nuevoDetalle = detailInvoice.filter(
      producto => producto.id !== row.id
    )
    setDetailInvoice(nuevoDetalle)
    let subTotal = total - parseInt(row.price)
    setTotal(subTotal)
  }

  const handlerAddProductDetail = (item, price) => {
    setDetailInvoice([
      ...detailInvoice,
      {
        id: item.sku,
        name_product: item.producto,
        qty: qty,
        price: price,
      },
    ])
  }

  const handlerReset = () => {
    setState({
      payment_method: "",
      customer: {},
    })
    setCustomersGet([])
    setProductsGet([])
    setDetailInvoice([])
    setViewSearchProduct(false)
  }

  const handleChange = event => {
    const name = event.target.name

    setState({
      ...state,
      [name]: event.target.value,
    })
  }

  useEffect(() => {
    if (getDataOrder) {
      getData(`${urlGetIdOrder}/bussinessordersforid`, callbackResponseGetOrder)
    }
  }, [getDataOrder, urlGetIdOrder])

  return (
    <>
      <div className="flex flex-col mt-10 px-3 w-full sm:w-full justify-between">
        <form
          className="flex flex-col sm:w-full justify-start items-start border-2 p-2 md:p-5 border-gray-500 rounded-xl"
          onSubmit={handleSubmit(onSubmit)}
        >
          <div className="flex flex-col">
            <div className="flex flex-wrap  md:flex-wrap justify-start items-start my-2">
              <p className="m-0 mt-2">Orden: </p>
              <p className="m-0 mt-2 font-semibold">{` 00${numberOrder}`}</p>
            </div>
            <div className="flex flex-wrap  md:flex-row lg:flex-row justify-start items-start my-2">
              <p className="m-0 mt-2">Factura</p>
              <input
                autoFocus
                tabindex="0"
                className="border-2 border-gray-400 rounded-md m-1  p-1"
                placeholder="Factura"
                {...register("invoice_number", {
                  maxLength: 50,
                })}
              />
            </div>
          </div>

          <div className="flex flex-wrap sm:flex-col justify-start items-start my-2">
            <div className="w-full flex flex-wrap">
              <p className="m-0 mt-2">Cliente</p>
              <input
                tabindex="-1"
                className="border-2 border-gray-400  rounded-md m-1  p-1"
                placeholder="Buscar..."
                onChange={e => handlerSearchCustomer(e)}
                // {...register("id_customer", { required: true, maxLength: 50 })}
              />
            </div>
            {errorCustomerReque && (
              <p className="text-sm text-red-700">El cliente es requerido</p>
            )}
            {state.customer !== {} && (
              <p className="font-semibold text-lg text-blue-900">
                {state.customer.full_name}
              </p>
            )}

            <div className="flex flex-col justify-start items-start">
              {customersGet &&
                customersGet.map((item, index) => (
                  <p
                    key={index}
                    className="flex flex-row justify-center items-center"
                  >
                    <Checkbox
                      onClick={() => handlerSelectCustmer(item)}
                      color="primary"
                      inputProps={{ "aria-label": "secondary checkbox" }}
                    />
                    <strong className="mr-2">
                      {item.identification_number}
                    </strong>{" "}
                    {` ${item.full_name} `}
                  </p>
                ))}
            </div>
          </div>

          {viewSearchProduct && (
            <div className="flex flex-wrap sm:flex-row md:flex-row lg:flex-row justify-start items-center my-2">
              <p className="m-0">Producto</p>
              <input
                tabindex="-2"
                className="border-2 border-gray-400 rounded-md m-1 p-1"
                placeholder="Buscar..."
                onChange={e => handlerSearchProduct(e)}
                // {...register("id_customer", { required: true, maxLength: 50 })}
              />

              <div className="flex flex-col justify-start items-start">
                {productsGet &&
                  productsGet.map((item, index) => (
                    <div key={index} className="flex flex-col w-full">
                      <p className="m-0 text-base text-left font-semibold">
                        {` ${item.producto} `}
                      </p>
                      <div className="flex flex-row justify-between">
                        <div className="my-1 sm:m-2 flex flex-col justify-end items-start">
                          <p className="m-0 text-lg flex flex-row justify-center items-center">
                            {`$ ${item.costo}`}
                          </p>
                          <p
                            className="text-blue-800"
                            style={{ fontSize: "10px" }}
                          >
                            Costo
                          </p>
                        </div>
                        <div className="my-1 sm:m-2">
                          <input
                            className="border-2 border-gray-400 rounded-md w-20 text-xl"
                            name="price"
                            placeholder="$"
                            defaultValue={calculatefinalcost(
                              customerType,
                              item.costo
                            )}
                          />

                          <p
                            className="text-blue-800"
                            style={{ fontSize: "10px" }}
                          >
                            Precio final
                          </p>
                        </div>
                        <div className="my-1 sm:m-2">
                          <input
                            type="number"
                            className="border-2 border-gray-400 rounded-md w-12 text-xl"
                            name="qty"
                            placeholder=""
                            onChange={e => setQty(parseInt(e.target.value))}
                          />
                          <AddCircleIcon
                            className=""
                            style={{ fontSize: 30 }}
                            onClick={() =>
                              handlerAddProductDetail(
                                item,
                                calculatefinalcost(customerType, item.costo)
                              )
                            }
                          />
                          <p
                            className="text-blue-800"
                            style={{ fontSize: "10px" }}
                          >
                            Cantidad del producto
                          </p>
                        </div>
                      </div>
                    </div>
                  ))}
              </div>
            </div>
          )}

          <FormControl variant="outlined" className={classes.formControl}>
            <InputLabel htmlFor="outlined-age-native-simple">
              Método de pago
            </InputLabel>
            <Select
              native
              onChange={e => handleChange(e)}
              label="Método de pago"
              inputProps={{
                name: "payment_method",
              }}
            >
              <option aria-label="None" value="" />
              <option value={"EFE"}>Efectivo</option>
              <option value={"TRA"}>Transferencia</option>
              <option value={"CHE"}>Cheque</option>
            </Select>
          </FormControl>

          <div className="flex flex-wrap sm:flex-row md:flex-row lg:flex-row justify-start items-center my-2">
            <p className="m-0">Observaciones</p>

            <textarea
              className="border-b-4 border-2 w-full border-gray-400 rounded-md m-0 my-3 px-2 h-16 shadow-lg"
              name="observations"
              placeholder="Observaciones"
              // onChange={e => handleTextarea(e)}
              {...register("observations", { maxLength: 200 })}
            />
          </div>

          <div className="w-full flex flex-row justify-between">
            <div className="mx-2 my-2">
              <Button
                disabled={diabledButton}
                variant="contained"
                onClick={handlerReset}
                color="secondary"
              >
                Cancelar
              </Button>
            </div>
            <div className="mx-2 my-2">
              <Button
                disabled={diabledButton}
                type="submit"
                variant="contained"
                color="primary"
              >
                Guardar
              </Button>
            </div>
          </div>
        </form>
        {errorDetailMayior && (
          <p className="text-sm text-red-700">
            El pedido debe tener al menos un producto
          </p>
        )}
        <div className="mt-3 w-full ">
          <TableContainer className="p-3" component={Paper}>
            <Table
              className={classes.table}
              size="small"
              aria-label="a dense table"
            >
              <TableHead>
                <TableRow>
                  <TableCell>Producto</TableCell>
                  <TableCell align="right">Cantidad</TableCell>
                  <TableCell align="right">Precion Unidad</TableCell>
                  <TableCell align="right">Subtotal</TableCell>
                  <TableCell align="right">Eliminar</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {detailInvoice.map(row => (
                  <TableRow key={row.name}>
                    <TableCell component="th" scope="row">
                      {row.name_product}
                    </TableCell>
                    <TableCell align="right">{row.qty}</TableCell>
                    <TableCell align="right">{row.price}</TableCell>
                    <TableCell align="right">
                      {parseInt(row.qty * row.price)}{" "}
                    </TableCell>
                    <TableCell align="right">
                      <DeleteForeverIcon
                        className=""
                        style={{ fontSize: 20 }}
                        onClick={() => handlerQuitarProducto(row)}
                      />
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </div>
      </div>
      {messageSucces && (
        <div className={classes.root}>
          <Alert severity="success">
            <AlertTitle>Excelente</AlertTitle>
            Producto creado — <strong>exitosamente!</strong>
          </Alert>
        </div>
      )}
      {messageError && (
        <div className={classes.root}>
          <Alert severity="error">
            <AlertTitle>Algo salio mal</AlertTitle>
            El producto no pudo ser creado —{" "}
            <strong>intenta nuevamente!</strong>
          </Alert>
        </div>
      )}
    </>
  )
}
