import React from "react"
import { makeStyles } from "@material-ui/core/styles"
import Table from "@material-ui/core/Table"
import TableBody from "@material-ui/core/TableBody"
import TableCell from "@material-ui/core/TableCell"
import TableContainer from "@material-ui/core/TableContainer"
import TableHead from "@material-ui/core/TableHead"
import TableRow from "@material-ui/core/TableRow"
import Paper from "@material-ui/core/Paper"
import Checkbox from "@material-ui/core/Checkbox"

const useStyles = makeStyles({
  table: {
    minWidth: 200,
  },
})

export default function ProductsDetail({ dataTable }) {
  const classes = useStyles()
  const handlerConfirm = value => {
    console.log("guardando..." + value.nameprod)
  }

  return (
    <TableContainer component={Paper}>
      <Table className={classes.table} size="small" aria-label="a dense table">
        <TableHead>
          <TableRow>
            <TableCell align="center">Confirmar</TableCell>
            <TableCell align="center">Producto</TableCell>
            <TableCell align="center">Cantidad</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {dataTable.map(row => (
            <TableRow key={row.nameprod}>
              <TableCell align="center">
                {" "}
                <Checkbox
                  defaultChecked={false}
                  color="primary"
                  inputProps={{ "aria-label": "secondary checkbox" }}
                  onClick={() => {
                    handlerConfirm(row)
                  }}
                />
              </TableCell>
              <TableCell component="th" scope="row" align="center">
                {row.nameprod}
              </TableCell>
              <TableCell align="center">{row.quantity}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  )
}
