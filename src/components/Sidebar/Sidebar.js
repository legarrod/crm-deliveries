import React, { useState, useEffect, useCallback } from "react"
import clsx from "clsx"
import { makeStyles, useTheme } from "@material-ui/core/styles"
import Drawer from "@material-ui/core/Drawer"
import AppBar from "@material-ui/core/AppBar"
import Toolbar from "@material-ui/core/Toolbar"
import List from "@material-ui/core/List"
import CssBaseline from "@material-ui/core/CssBaseline"
import Typography from "@material-ui/core/Typography"
import Divider from "@material-ui/core/Divider"
import IconButton from "@material-ui/core/IconButton"
import MenuIcon from "@material-ui/icons/Menu"
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft"
import ChevronRightIcon from "@material-ui/icons/ChevronRight"
import ListItem from "@material-ui/core/ListItem"
import ListItemIcon from "@material-ui/core/ListItemIcon"
import ListItemText from "@material-ui/core/ListItemText"
import Badge from "@material-ui/core/Badge"
import ExportCSV from "../../utils/exportar"
import { Link } from "gatsby"
import { getData } from "../../api/AsyncHttpRequest"
import HomeIcon from "@material-ui/icons/Home"
import {
  FaJenkins,
  FaGrav,
  FaPalfed,
  FaPeopleCarry,
  FaChartLine,
  FaCartPlus,
  FaBook,
} from "react-icons/fa"

const drawerWidth = 240

const useStyles = makeStyles(theme => ({
  root: {
    display: "flex",
  },
  appBar: {
    backgroundColor: "#328039",
    color: "#FFFFFF",
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginRight: 36,
    color: "#FFFFFF",
  },
  itemCustom: { color: "#ffffff" },
  hide: {
    display: "none",
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: "nowrap",
  },
  drawerOpen: {
    backgroundColor: "#2C2C2C",
    color: "#ffffff",
    width: drawerWidth,
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    backgroundColor: "#2C2C2C",
    color: "#ffffff",
    transition: theme.transitions.create("width", {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: "hidden",
    width: theme.spacing(5) + 1,
    [theme.breakpoints.up("sm")]: {
      width: theme.spacing(7) + 1,
    },
  },
  customListem: { padding: "0" },
  toolbar: {
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end",
    padding: theme.spacing(0, 1),
    // necessary for content to be below app bar
    ...theme.mixins.toolbar,
  },
  content: {
    paddingTop: "35px",
    flexGrow: 1,
  },
}))

export default function Sidebar({ children }) {
  const [count, setCount] = useState(0)
  const [buscando, setBuscando] = useState(true)
  const classes = useStyles()
  const theme = useTheme()
  const [open, setOpen] = React.useState(false)
  const urlAllCustomers = `http://apismartgo.crmsystem.tech/public/bussinessorders/api`

  const handleDrawerOpen = () => {
    setOpen(true)
  }

  const handleDrawerClose = () => {
    setOpen(false)
  }

  const respponseCallback = useCallback(response => {
    setCount(parseInt(response[0]?.total))
    setBuscando(false)

    // setInterval(() => {
    //   refresh()
    // }, 600000)
  }, [])

  const refresh = () => {
    getData(`${urlAllCustomers}/bussinessorderstotal`, respponseCallback)
  }

  useEffect(() => {
    if (buscando) {
      getData(`${urlAllCustomers}/bussinessorderstotal`, respponseCallback)
    }
  }, [urlAllCustomers, respponseCallback, buscando])

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar
        position="fixed"
        className={clsx(classes.appBar, {
          [classes.appBarShift]: open,
        })}
      >
        <Toolbar>
          <IconButton
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            className={clsx(classes.menuButton, {
              [classes.hide]: open,
            })}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h4" noWrap>
            Sistema FrutGo
          </Typography>
        </Toolbar>
      </AppBar>
      <Drawer
        variant="permanent"
        className={clsx(classes.drawer, {
          [classes.drawerOpen]: open,
          [classes.drawerClose]: !open,
        })}
        classes={{
          paper: clsx({
            [classes.drawerOpen]: open,
            [classes.drawerClose]: !open,
          }),
        }}
      >
        <div className={classes.toolbar}>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === "rtl" ? (
              <ChevronRightIcon />
            ) : (
              <ChevronLeftIcon className={classes.itemCustom} />
            )}
          </IconButton>
        </div>
        <Divider />
        <List>
          <Link to="/">
            <ListItem button className={classes.customListem}>
              <ListItemIcon>
                <IconButton aria-label="show 4 new mails" color="inherit">
                  <HomeIcon className={classes.itemCustom} />
                </IconButton>
              </ListItemIcon>
              <ListItemText primary={"Inicio"} />
            </ListItem>
          </Link>

          <Link to="/orders">
            <ListItem button className={classes.customListem}>
              <ListItemIcon>
                <IconButton aria-label="show 4 new mails" color="inherit">
                  <Badge badgeContent={count} color="secondary">
                    <FaCartPlus className={classes.itemCustom} />
                  </Badge>
                </IconButton>
              </ListItemIcon>
              <ListItemText primary={"Pedidos"} />
            </ListItem>
          </Link>

          <Link to="/statistics">
            <ListItem button className={classes.customListem}>
              <ListItemIcon>
                <IconButton aria-label="show 4 new mails" color="inherit">
                  <FaChartLine className={classes.itemCustom} />
                </IconButton>
              </ListItemIcon>
              <ListItemText primary={"Estadisiticas"} />
            </ListItem>
          </Link>

          <Link to="/customer">
            <ListItem button className={classes.customListem}>
              <ListItemIcon>
                <IconButton aria-label="show 4 new mails" color="inherit">
                  <FaJenkins className={classes.itemCustom} />
                </IconButton>
              </ListItemIcon>
              <ListItemText primary={"Clientes"} />
            </ListItem>
          </Link>

          <Link to="/products">
            <ListItem button className={classes.customListem}>
              <ListItemIcon>
                <IconButton aria-label="show 4 new mails" color="inherit">
                  <FaPalfed className={classes.itemCustom} />
                </IconButton>
              </ListItemIcon>
              <ListItemText primary={"Productos"} />
            </ListItem>
          </Link>

          <Link to="/vendors">
            <ListItem button className={classes.customListem}>
              <ListItemIcon>
                <IconButton aria-label="show 4 new mails" color="inherit">
                  <FaGrav className={classes.itemCustom} />
                </IconButton>
              </ListItemIcon>
              <ListItemText primary={"Proveedores"} />
            </ListItem>
          </Link>

          <Link to="/deliver">
            <ListItem button className={classes.customListem}>
              <ListItemIcon>
                <IconButton aria-label="show 4 new mails" color="inherit">
                  <FaPeopleCarry className={classes.itemCustom} />
                </IconButton>
              </ListItemIcon>
              <ListItemText primary={"Entrega"} />
            </ListItem>
          </Link>
        </List>
        <Divider className={classes.itemCustom} style={{ color: "#ffffff" }} />
        <List>
          <Link to="/documentation">
            <ListItem button className={classes.customListem}>
              <ListItemIcon>
                <IconButton aria-label="show 4 new mails" color="inherit">
                  <FaBook className={classes.itemCustom} />
                </IconButton>
              </ListItemIcon>
              <ListItemText primary={"Documentación"} />
            </ListItem>
          </Link>
          <ListItem button className={classes.customListem}>
            <ListItemIcon>
              <IconButton aria-label="show 4 new mails" color="inherit">
                <ExportCSV
                  fileName={"Reporte"}
                  className={classes.itemCustom}
                />
              </IconButton>
            </ListItemIcon>
          </ListItem>
        </List>
      </Drawer>
      <main className={classes.content}>{children}</main>
    </div>
  )
}
