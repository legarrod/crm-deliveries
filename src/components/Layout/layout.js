import React from "react"
import PropTypes from "prop-types"
import Sidebar from "../Sidebar/Sidebar"
import Header from "../Header/header"
//import "./layout.css"

const Layout = ({ children }) => {
  return (
    <>
      {/* <Header siteTitle={"CRM SmartGo"} /> */}
      <Sidebar />
      <div className="p-0 h-screen flex flex-col pt-9 justify-between">
        <main className="ml-4">{children}</main>

        <footer
          style={{
            padding: `20px 30px 10px 30px`,
            background: `#000000`,
            color: `#ffffff`,
            marginTop: `0`,
          }}
        >
          © {new Date().getFullYear()}, developed by
          {` `}
          <a href="#!">Luis E. Garcia</a>
        </footer>
      </div>
    </>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
