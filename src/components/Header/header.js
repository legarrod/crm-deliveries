import React, { useEffect, useState, useCallback } from "react"
import { fade, makeStyles } from "@material-ui/core/styles"
import AppBar from "@material-ui/core/AppBar"
import Toolbar from "@material-ui/core/Toolbar"
import IconButton from "@material-ui/core/IconButton"
import Typography from "@material-ui/core/Typography"
// import InputBase from '@material-ui/core/InputBase';
import Badge from "@material-ui/core/Badge"
import MenuItem from "@material-ui/core/MenuItem"
import Menu from "@material-ui/core/Menu"
// import SearchIcon from '@material-ui/icons/Search';
//import AccountCircle from "@material-ui/icons/AccountCircle"
import MoreIcon from "@material-ui/icons/MoreVert"
import { Link } from "gatsby"
import { getData } from "../../api/AsyncHttpRequest"
import ExportCSV from "../../utils/exportar"
import {
  FaJenkins,
  FaGrav,
  FaPalfed,
  FaPeopleCarry,
  FaSyncAlt,
  FaChartLine,
} from "react-icons/fa"

const useStyles = makeStyles(theme => ({
  grow: {
    flexGrow: 1,
  },
  menu: {
    backgroundColor: "blue",
    color: "#59D741",
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    display: "none",
    fontWeight: "bold",
    [theme.breakpoints.up("sm")]: {
      display: "block",
    },
  },
  search: {
    position: "relative",
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    "&:hover": {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: "100%",
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing(3),
      width: "auto",
    },
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
  },
  inputRoot: {
    color: "inherit",
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create("width"),
    width: "100%",
    [theme.breakpoints.up("md")]: {
      width: "20ch",
    },
  },
  sectionDesktop: {
    display: "none",
    [theme.breakpoints.up("md")]: {
      display: "flex",
    },
  },
  sectionMobile: {
    display: "flex",
    [theme.breakpoints.up("md")]: {
      display: "none",
    },
  },
}))

export default function Header() {
  const classes = useStyles()
  const [anchorEl, setAnchorEl] = React.useState(null)
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = React.useState(null)
  const urlAllCustomers = `http://apismartgo.crmsystem.tech//public/bussinessorders/api`
  const [count, setCount] = useState(0)
  const [buscando, setBuscando] = useState(true)
  const isMenuOpen = Boolean(anchorEl)
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl)

  // const handleProfileMenuOpen = event => {
  //   setAnchorEl(event.currentTarget)
  // }

  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null)
  }

  const handleMenuClose = () => {
    setAnchorEl(null)
    handleMobileMenuClose()
  }

  const handleMobileMenuOpen = event => {
    setMobileMoreAnchorEl(event.currentTarget)
  }

  const menuId = "primary-search-account-menu"
  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{ vertical: "top", horizontal: "right" }}
      id={menuId}
      keepMounted
      transformOrigin={{ vertical: "top", horizontal: "right" }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      <MenuItem onClick={handleMenuClose}>Perfil</MenuItem>
    </Menu>
  )

  const mobileMenuId = "primary-search-account-menu-mobile"
  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{ vertical: "top", horizontal: "right" }}
      id={mobileMenuId}
      keepMounted
      transformOrigin={{ vertical: "top", horizontal: "right" }}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
      <MenuItem>
        <IconButton
          aria-label="show 4 new mails"
          color="inherit"
          onClick={() => refresh()}
        >
          <FaSyncAlt />
        </IconButton>
        <p>Refrescar</p>
      </MenuItem>

      <Link to="/customer">
        <MenuItem>
          <IconButton aria-label="show 4 new mails" color="inherit">
            <FaJenkins />
          </IconButton>
          <p>Clientes</p>
        </MenuItem>
      </Link>

      <Link to="/products">
        <MenuItem>
          <IconButton aria-label="show 4 new mails" color="inherit">
            <FaPalfed />
          </IconButton>
          <p>Productos</p>
        </MenuItem>
      </Link>

      <Link to="/vendors">
        <MenuItem>
          <IconButton aria-label="show 4 new mails" color="inherit">
            <FaGrav />
          </IconButton>
          <p>Proveedores</p>
        </MenuItem>
      </Link>
      <Link to="/orders">
        <MenuItem>
          <IconButton aria-label="show 11 new notifications" color="inherit">
            <Badge badgeContent={count} color="secondary">
              <FaPeopleCarry />
            </Badge>
          </IconButton>
          <p>Pedidos</p>
        </MenuItem>
      </Link>

      <Link to="/statistics">
        <MenuItem>
          <IconButton aria-label="show 11 new notifications" color="inherit">
            <Badge color="secondary">
              <FaChartLine />
            </Badge>
          </IconButton>
          <p>Estadisticas</p>
        </MenuItem>
      </Link>
      <MenuItem>
        <IconButton aria-label="show 11 new notifications" color="inherit">
          <Badge color="secondary">
            <ExportCSV fileName={"Reporte"} />
          </Badge>
        </IconButton>
        <p>Excel</p>
      </MenuItem>
      {/* <MenuItem onClick={handleProfileMenuOpen}>
        <IconButton
          aria-label="account of current user"
          aria-controls="primary-search-account-menu"
          aria-haspopup="true"
          color="inherit"
        >
          <AccountCircle />
        </IconButton>
        <p>Perfil</p>
      </MenuItem> */}
    </Menu>
  )
  const respponseCallback = useCallback(response => {
    setCount(parseInt(response[0]?.total))
    setBuscando(false)
  }, [])

  const refresh = () => {
    getData(`${urlAllCustomers}/bussinessorderstotal`, respponseCallback)
  }

  useEffect(() => {
    if (buscando) {
      getData(`${urlAllCustomers}/bussinessorderstotal`, respponseCallback)
    }
  }, [urlAllCustomers, respponseCallback, buscando])

  return (
    <div className={`${classes.grow}`}>
      <AppBar
        classes={{
          root: classes.menu,
        }}
      >
        <Toolbar>
          <Link to="/">
            <Typography className={classes.title} variant="h6" noWrap>
              Sistema FrutGo
            </Typography>
          </Link>

          <div className={classes.grow} />
          <div className={classes.sectionDesktop}>
            <IconButton
              aria-label="show 4 new mails"
              color="inherit"
              onClick={() => refresh()}
            >
              <FaSyncAlt />
            </IconButton>

            <Link to="/vendors">
              <IconButton aria-label="show 4 new mails" color="inherit">
                <FaGrav />
              </IconButton>
            </Link>
            <Link to="/customer">
              <IconButton aria-label="show 4 new mails" color="inherit">
                <FaJenkins />
              </IconButton>
            </Link>

            <Link to="/products">
              <IconButton
                aria-label="show 17 new notifications"
                color="inherit"
              >
                <Badge color="secondary">
                  <FaPalfed />
                </Badge>
              </IconButton>
            </Link>

            <Link to="/orders">
              <IconButton
                aria-label="show 17 new notifications"
                color="inherit"
              >
                <Badge badgeContent={count} color="secondary">
                  <FaPeopleCarry />
                </Badge>
              </IconButton>
            </Link>

            {/* <IconButton
              edge="end"
              aria-label="account of current user"
              aria-controls={menuId}
              aria-haspopup="true"
              onClick={handleProfileMenuOpen}
              color="inherit"
            >
              <AccountCircle />
            </IconButton> */}
            <Link to="/statistics">
              <IconButton
                aria-label="show 17 new notifications"
                color="inherit"
              >
                <Badge color="secondary">
                  <FaChartLine />
                </Badge>
              </IconButton>
            </Link>
            <ExportCSV fileName={"Reporte"} />
          </div>
          <div className={classes.sectionMobile}>
            <IconButton
              aria-label="show more"
              aria-controls={mobileMenuId}
              aria-haspopup="true"
              onClick={handleMobileMenuOpen}
              color="inherit"
            >
              <MoreIcon />
            </IconButton>
          </div>
        </Toolbar>
      </AppBar>
      {renderMobileMenu}
      {renderMenu}
    </div>
  )
}
